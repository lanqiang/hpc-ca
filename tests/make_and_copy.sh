#!/bin/bash
module load papi
module load hdf5/1.8.12_intel
cd ../code/fvm_mpi
make clean
make -f Makefile.abel
cp calcium_sparks ../../tests
