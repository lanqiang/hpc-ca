#ifndef __BOUNDARYFLUXES_H
#define __BOUNDARYFLUXES_H

#include <mpi.h>

#include "types.h"

// Struct keeping parameters for fluxes
struct SERCA_model {

  REAL v_cyt_to_A_sr;
  REAL density;
  REAL scale;

  // The name of the boundary this flux exists on
  char boundary_name[MAX_SPECIES_NAME];

  // Parameters
  REAL params[3];

};

struct RyR_model {

  // State values
  unsigned int N;
  domain_id* s0;

  // Parameters
  REAL Kd_open;
  REAL k_min_open;
  REAL k_max_open;
  REAL n_open;

  REAL Kd_close;
  REAL k_min_close;
  REAL k_max_close;
  REAL n_close;

  REAL i_unitary;

  REAL t_close_ryrs;

  // The name of the boundary this flux exists on
  char boundary_name[MAX_SPECIES_NAME];

  // Parameters
  REAL params[1];

};

typedef struct RyR_model RyR_model_t;
typedef struct SERCA_model SERCA_model_t;

// Boundary flux struct
// FIXME: Everything in boundary fluxes are pretty hardcoded to serca and RyR 
// FXIME: fluxes. It would be nice to make this more flexible. With for example 
// FIXME: code generation...
struct BoundaryFluxes {
  
  // Flags for using ryr and serca in the model
  domain_id use_ryr, use_serca;
  
  // The RyR model
  RyR_model_t ryr;

  // The SERCA model
  SERCA_model_t serca;
  
  // The number of different boundary types
  domain_id num_flux_types;
  
};

typedef struct BoundaryFluxes BoundaryFluxes_t;

// Flux accomodated by the serca pump
REAL serca_flux(REAL dt, REAL h, REAL u0, REAL u1, REAL* params);

// Flux through a single open channel
REAL ryr_flux(REAL dt, REAL h, REAL u0, REAL u1, REAL* params);

// Initialize a RyR_model
void init_RyR_model(RyR_model_t* ryr);

// Initialize a SERCA_model
void init_SERCA_model(SERCA_model_t* serca);

// Evaluate the RyR states stochastically
void evaluate_RyR_stochastically(RyR_model_t* ryr, REAL t, REAL dt, 
                                 REAL* species_at_boundary);

// Return true if the ith ryr flux is open
domain_id open_RyR(RyR_model_t* ryr, int i);

// Initialize RyR states
void BoundaryFluxes_init_stochastic_boundaries(Species_t* species, arguments_t* arguments);

// Initialize RyR states using initial Ca concentration
void init_RyR_model_states_stochastically(RyR_model_t* ryr, REAL* species_at_boundary);

// Initialize RyR states using command line arguments
void init_RyR_model_states_deterministically(RyR_model_t* ryr, 
					     unsigned int number_init_ryrs,
					     int* open_ryrs);

// Construct a BoundaryFlux struct 
BoundaryFluxes_t* BoundaryFluxes_construct(Species_t* species, hid_t file_id, 
                                           arguments_t* arguments);

// Output initial data of BoundaryFluxes
void BoundaryFluxes_output_init_data(Species_t* species, arguments_t* arguments);

// Destroy BoundaryFlux struct
void BoundaryFluxes_destruct(BoundaryFluxes_t* fluxes);

// Init voxels for each flux
void BoundaryFluxes_init_flux_voxels(BoundaryFluxes_t* fluxes, Geometry_t* geom, 
				     domain_id flux_id, domain_id boundary_id);
#endif
