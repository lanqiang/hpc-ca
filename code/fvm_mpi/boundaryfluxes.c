#include <assert.h>
#include <math.h>

#include "mtwist.h"
#include "utils.h"
#include "geometry.h"
#include "species.h"

#include "boundaryfluxes.h"

//-----------------------------------------------------------------------------
REAL serca_flux(REAL dt, REAL h, REAL u0, REAL u1, REAL* params)
{
  
  REAL serca_scale, Ca_SR_2, Ca_i_2, T, c0_T, v_cyt_to_A_sr, density, scale;

  // Hardcode temperature
  T = 298.;

  // Get parameters
  v_cyt_to_A_sr = params[0];
  density = params[1];
  scale = params[2];

  // Calculate temperature dependent part
  c0_T = -36./(87680.*exp(0.12*(-134.*T + 29600)/T) + 0.00133);

  // Assign local variable from caller
  Ca_i_2 = u0;
  Ca_SR_2  = u1;

  // Raise to the power of 2
  Ca_SR_2 *= Ca_SR_2;
  Ca_i_2 *= Ca_i_2;
  
  // Calculate scaling factors
  serca_scale = 1e-3;           // from s to ms
  serca_scale *= 2;             // from cycles to Ca
  serca_scale *= density;       // From # to umole/(cytosole l)
  serca_scale *= v_cyt_to_A_sr; // From density to area distribution
  serca_scale *= scale;         // Arbitrary scale factor
  serca_scale /= h;             // Making the area integration dA/dV

  // Applying CaSR and CaCyt dependencies
  return serca_scale*(571000000.*Ca_i_2 + c0_T*Ca_SR_2)/        \
    (106700000.*Ca_i_2 + 182.5*Ca_SR_2 + 5.35)*dt;
  
}
//-----------------------------------------------------------------------------
REAL ryr_flux(REAL dt, REAL h, REAL u0, REAL u1, REAL* params)
{

  // Flux is described as:
  //
  //   J_ryr = g_c/h^3(u0-u1)
  //
  // where:
  //
  //   g_c = K*i_c/(U0-U1)
  //
  // Here i_c is the unitary current through an open channel in pA, when it opens,
  // U1 and U0 is the initial concentration when this current is measured, and K 
  // a constant:
  //
  //   K = 10^15/(z*F)
  //
  // Here z is the valence of the current (2) and F Faraday's constant. 
  //
  // So with explicit Euler we have:
  // 
  //   J_ryr = g_c/h^3*(u0-u1)
  //   u0 -= dt*J_RyR;
  //   u1 += dt*J_RyR
  //
  // With K so large this scheme will not be stable so we need to solve it 
  // analytically with the following scheme:
  //
  //   c0 = (u1 + u0)/2
  //   c1 = (u1 - u0)/2
  //   u1 = c0 + c1*K_0 
  //   u0 = c0 - c1*K_0
  //
  // where K_0 is given by:
  //
  //   K_0 = exp(-2*dt*g_c/h^3)

  //REAL c0, c1;
  REAL i_c, K_0;

  // g_c = 10^15/(z*F)*i_c/(U1-U0)
  // K_0 = exp(-2*dt*g_c/h**3)
  i_c = params[0];
  K_0 = exp(-2*dt*1e15/(2*96485.)*i_c/(1300.-0.1)/(h*h*h));
  
  // The resulting analytic flux
  //printf("u0: %f; u1:%f\n", u0, u1);
  return (u0-u1)*(1-K_0)/2;

  //c0 = (*u1 + *u0)/2;
  //c1 = (*u1 - *u0)/2;
  //*u0 = c0 - c1*K_0;
  //*u1 = c0 + c1*K_0;

}
//-----------------------------------------------------------------------------
void init_RyR_model(RyR_model_t* ryr)
{

  ryr->N = 0;

  ryr->Kd_open = 127.92;
  ryr->k_min_open = 1e-4;
  ryr->k_max_open = 0.7;
  ryr->n_open = 2.8;

  ryr->Kd_close = 62.5;
  ryr->k_min_close = 0.9;
  ryr->k_max_close = 10.;
  ryr->n_close = -0.5;

  ryr->t_close_ryrs = -1.;

  ryr->i_unitary = 0.5;
  ryr->params[0] = ryr->i_unitary;
}
//-----------------------------------------------------------------------------
domain_id open_RyR(RyR_model_t* ryr, int i)
{
  assert(i<ryr->N);
  return ryr->s0[i];
}
//-----------------------------------------------------------------------------
void init_SERCA_model(SERCA_model_t* serca)
{
  serca->v_cyt_to_A_sr = 307.;
  serca->density = 75.;
  serca->scale = 1.0;

  serca->params[0] = serca->v_cyt_to_A_sr;
  serca->params[1] = serca->density;
  serca->params[2] = serca->scale;

}
//-----------------------------------------------------------------------------
void init_RyR_model_states_stochastically(RyR_model_t* ryr, REAL* species_at_boundary)
{

  unsigned int i;
  REAL ko, kc, frac_s0, c0;

  if (ryr->N==0)
    return;

  // Assume the state arrays has been constructed
  assert(ryr->s0);
  
  for (i=0; i<ryr->N; i++)
  {

    // Get species at outlet of the boundary
    c0 = species_at_boundary[i*2+1];

    // Compute intermediates for the stochastic evaluation
    ko = fmax(fmin(pow(c0/ryr->Kd_open, ryr->n_open), ryr->k_max_open), ryr->k_min_open);
    kc = fmax(fmin(pow(c0/ryr->Kd_close, ryr->n_close), ryr->k_max_close), ryr->k_min_close);
    frac_s0 = ko/(kc+ko);

    // Stochastic evaluation
    ryr->s0[i] = mt_ldrand()<=frac_s0 ? 1 : 0;

  }
}
//-----------------------------------------------------------------------------
void init_RyR_model_states_deterministically(RyR_model_t* ryr, 
					     unsigned int number_init_ryrs,
					     int* open_ryrs)
{
  
  if (ryr->N==0)
    return;

  unsigned int i;
  
  // Assume the state arrays has been constructed
  assert(ryr->s0);
  
  for (i=0; i<ryr->N; i++)
    ryr->s0[i] = 0;

  for (i=0; i<number_init_ryrs; i++)
    ryr->s0[open_ryrs[i]] = 1;

}
//-----------------------------------------------------------------------------
void evaluate_RyR_stochastically(RyR_model_t* ryr, REAL t, REAL dt, 
                                 REAL* species_at_boundary)
{
  
  unsigned int i;
  REAL ko, kc, c0;

  if (ryr->N==0)
    return;

  // Assume the state arrays has been constructed
  assert(ryr->s0);
  
  for (i=0; i<ryr->N; i++)
  {

    // Check for forcing ryr to close (or be open)
    if (ryr->t_close_ryrs>0)
    {

      // Check for close time
      if (ryr->t_close_ryrs<=t+dt)
        ryr->s0[i] = 0;
      
      // Do not continue to stochastic evaluation
      continue;
    }

    // Get species at outlet of the boundary
    c0 = species_at_boundary[i*2+1];

    // If state 0 in "open" state
    if (ryr->s0[i])
    {
      kc = fmax(fmin(pow(c0/ryr->Kd_close, ryr->n_close), ryr->k_max_close), 
                ryr->k_min_close);
      ryr->s0[i] = mt_ldrand()<=(kc*dt) ? 0 : 1;
    }
    else
    {
      ko = fmax(fmin(pow(c0/ryr->Kd_open, ryr->n_open), ryr->k_max_open), 
                ryr->k_min_open);;
      ryr->s0[i] = mt_ldrand()<=(ko*dt) ? 1 : 0;
    }
  }
}
//-----------------------------------------------------------------------------
BoundaryFluxes_t* BoundaryFluxes_construct(Species_t* species, hid_t file_id, 
                                           arguments_t* arguments)
{
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;

  BoundaryFluxes_t* fluxes = mpi_malloc(comm, sizeof(BoundaryFluxes_t));
  fluxes->ryr.s0 = NULL;

  // Initalize ryr and serca models with default parameters
  init_RyR_model(&fluxes->ryr);
  init_SERCA_model(&fluxes->serca);

  // Read usage flags
  read_h5_attr(comm, file_id, "/", "use_ryr", &fluxes->use_ryr);
  read_h5_attr(comm, file_id, "/", "use_serca", &fluxes->use_serca);
  
  // Calculate the number of fluxes
  fluxes->num_flux_types = 0;
  fluxes->num_flux_types += fluxes->use_ryr;
  fluxes->num_flux_types += fluxes->use_serca;

  // Return struct if no boundary fluxes
  if (fluxes->num_flux_types==0)
    return fluxes;

  // Initialize ryr data
  if (fluxes->use_ryr)
  {

    // Read RyR attributes from file
    read_h5_attr(comm, file_id, "/ryr", "Kd_open", &fluxes->ryr.Kd_open);
    read_h5_attr(comm, file_id, "/ryr", "k_min_open", &fluxes->ryr.k_min_open);
    read_h5_attr(comm, file_id, "/ryr", "k_max_open", &fluxes->ryr.k_max_open);
    read_h5_attr(comm, file_id, "/ryr", "n_open", &fluxes->ryr.n_open);

    read_h5_attr(comm, file_id, "/ryr", "Kd_close", &fluxes->ryr.Kd_close);
    read_h5_attr(comm, file_id, "/ryr", "k_min_close", &fluxes->ryr.k_min_close);
    read_h5_attr(comm, file_id, "/ryr", "k_max_close", &fluxes->ryr.k_max_close);
    read_h5_attr(comm, file_id, "/ryr", "n_close", &fluxes->ryr.n_close);

    read_h5_attr(comm, file_id, "/ryr", "i_unitary", &fluxes->ryr.i_unitary);
    read_h5_attr(comm, file_id, "/ryr", "boundary", &fluxes->ryr.boundary_name);
    
    // Update flux parameters
    fluxes->ryr.params[0] = fluxes->ryr.i_unitary;
    fluxes->ryr.t_close_ryrs = arguments->t_close_ryrs;

    // Allocate states only on rank 0 process
    if (geom->mpi_info.rank == 0)
    {
      
      // Allocate global RyR states
      fluxes->ryr.N  = geom->boundary_size[Geometry_get_boundary_id(geom, "ryr")];
      fluxes->ryr.s0 = mpi_malloc(comm, sizeof(domain_id)*fluxes->ryr.N);
      
    }
  }

  // Initialize serca data
  if (fluxes->use_serca)
  {

    // Read SERCA attributes from file
    read_h5_attr(comm, file_id, "/serca", "v_cyt_to_A_sr", &fluxes->serca.v_cyt_to_A_sr);
    read_h5_attr(comm, file_id, "/serca", "density", &fluxes->serca.density);
    read_h5_attr(comm, file_id, "/serca", "scale", &fluxes->serca.scale);
    read_h5_attr(comm, file_id, "/serca", "boundary", &fluxes->serca.boundary_name);
    
    // Update flux parameters
    fluxes->serca.params[0] = fluxes->serca.v_cyt_to_A_sr;
    fluxes->serca.params[1] = fluxes->serca.density;
    fluxes->serca.params[2] = fluxes->serca.scale;

  }

  return fluxes;
}
//-----------------------------------------------------------------------------
void BoundaryFluxes_init_stochastic_boundaries(Species_t* species, arguments_t* arguments)
{
  
  // FIXME: Make this more flexible! Now it is hardcoded to RyR

  unsigned int dbi;
  Geometry_t* geom = species->geom;
  BoundaryFluxes_t* fluxes = species->boundary_fluxes;
  
  // Communicate species values at discrete boundaries to processor 0
  Species_communicate_values_at_discrete_boundaries(species);
   
  // Initialize ryr data
  if (fluxes->use_ryr && geom->num_global_discrete_boundaries>0)
  {
    
    if (geom->mpi_info.rank == 0)
    {
    
      // Initiate state values
      if (arguments->number_init_ryrs>0)
      {
        init_RyR_model_states_deterministically(&fluxes->ryr, arguments->number_init_ryrs, 
                                                arguments->open_ryrs);
      }
      else
      {
    
        // FIXME: Do not hard code the dbi to 0
        dbi = 0;
        init_RyR_model_states_stochastically(&fluxes->ryr, \
            geom->species_values_at_local_discrete_boundaries_correct_order_rank_0[dbi]);
        
      }
    }
  }

  // Communicate the openness of the discrete boundaries from rank 0 to other processes
  Species_communicate_openness_of_discrete_boundaries(species);

}
//-----------------------------------------------------------------------------
void BoundaryFluxes_output_init_data(Species_t* species, arguments_t* arguments)
{
  int i;
  BoundaryFluxes_t* fluxes = species->boundary_fluxes;
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;
  
  if (!(fluxes->use_ryr || fluxes->use_serca))
    return;

  mpi_printf0(comm, "\n");
  mpi_printf0(comm, "Boundary fluxes:\n");
  mpi_printf0(comm, "-----------------------------------------------------------------------------\n");

  if (fluxes->use_ryr)
  {
    mpi_printf0(comm, "  RyR:\n");
    mpi_printf0(comm, "  Open:  kd: %4.2f, N: %.2f, k_min: %.2f, k_max: %.2f\n", 
                fluxes->ryr.Kd_open, fluxes->ryr.n_open, fluxes->ryr.k_min_open, 
                fluxes->ryr.k_max_open);
    mpi_printf0(comm, "  Close:  kd: %4.2f, N: %.2f, k_min: %.2f, k_max: %.2f\n", 
                fluxes->ryr.Kd_close, fluxes->ryr.n_close, fluxes->ryr.k_min_close, 
                fluxes->ryr.k_max_close);

    mpi_printf0(comm, "  Flux:        i_c: %.2f pA\n", fluxes->ryr.i_unitary);

    if (geom->mpi_info.rank == 0)
    {
      int open = 0;
      for (i=0; i<fluxes->ryr.N; i++)
        open += open_RyR(&fluxes->ryr, i);
      mpi_printf0(comm, "  Num open RyRs: %d/%d\n", open, fluxes->ryr.N);
    }
    mpi_printf0(comm, "\n");
  }
  
  if (fluxes->use_serca)
  {
    mpi_printf0(comm, "  SERCA:\n");
    mpi_printf0(comm, "  v_cyt_to_A_sr: %4.2g\n", fluxes->serca.v_cyt_to_A_sr);
    mpi_printf0(comm, "        density: %4.2g\n", fluxes->serca.density);
    mpi_printf0(comm, "          scale: %4.2g\n", fluxes->serca.scale);
  }
}
//-----------------------------------------------------------------------------
void BoundaryFluxes_destruct(BoundaryFluxes_t* fluxes)
{

  if (fluxes->num_flux_types==0)
    return;

  free(fluxes->ryr.s0);
  free(fluxes);
  
}
//-----------------------------------------------------------------------------
