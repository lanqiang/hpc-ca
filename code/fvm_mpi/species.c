#include <hdf5.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <assert.h>

#include "species.h"
#include "boundaryfluxes.h"
#include "utils.h"
#include <omp.h>
#define __PAPI__
#ifdef __PAPI__
#include <papi.h>
#endif

#include "immintrin.h"

//-----------------------------------------------------------------------------
Species_t* Species_construct(Geometry_t* geom, arguments_t* arguments)
{
  
  size_t i, bi, di, si, dsi, dim, dim1, dim2, n[NDIMS];
  MPI_Comm comm = geom->mpi_info.comm;
  Species_t* species = mpi_malloc(comm, sizeof(Species_t));
  species->geom = geom;

  // Start reading model file
  hid_t plist_id = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(plist_id, comm, MPI_INFO_NULL);
  
  // Create a new file collectively and release property list identifier.
  hid_t file_id = H5Fopen(arguments->model_file, H5F_ACC_RDONLY, plist_id);
  H5Pclose(plist_id);

  // Read domains and compare with geom
  domain_id num_domains;
  read_h5_attr(comm, file_id, "/", "num_domains", &num_domains);

  if (num_domains!=geom->num_domains)
    mpi_printf_error(comm, "*** ERROR: Expected the same number of domains in geometry "\
		     "and model files. %d!=%d\n", geom->num_domains, num_domains);
  
  // Allocate information about species that are domain specific
  species->num_diffusive = mpi_malloc(comm, sizeof(domain_id)*geom->num_domains);
  species->diffusive = mpi_malloc(comm, sizeof(domain_id*)*geom->num_domains);
  species->sigma = mpi_malloc(comm, sizeof(REAL*)*geom->num_domains);
  species->init = mpi_malloc(comm, sizeof(REAL*)*geom->num_domains);
  species->num_buffers = mpi_malloc(comm, sizeof(domain_id)*geom->num_domains);
  species->k_off = mpi_malloc(comm, sizeof(REAL*)*geom->num_domains); 
  species->k_on = mpi_malloc(comm, sizeof(REAL*)*geom->num_domains); 
  species->tot = mpi_malloc(comm, sizeof(REAL*)*geom->num_domains);
  species->bsp0 = mpi_malloc(comm, sizeof(domain_id*)*geom->num_domains); 
  species->bsp1 = mpi_malloc(comm, sizeof(domain_id*)*geom->num_domains);
  species->local_domain_num = mpi_malloc(comm, sizeof(long)*geom->num_domains);
  memfill_long(species->local_domain_num, geom->num_domains, 0);

  // Read number of species
  read_h5_attr(comm, file_id, "/", "num_species", &species->num_species);
  
  // Allocate num fixed domain species
  species->num_fixed_domain_species = mpi_malloc(comm, sizeof(size_t)*species->num_species);
  memfill_size_t(species->num_fixed_domain_species, species->num_species, 0);
  species->fixed_domain_species = mpi_malloc(comm, sizeof(size_t*)*species->num_species);
  for (i=0; i<species->num_species; i++)
    species->fixed_domain_species[i] = NULL;

  // Assign volume of species voxel
  species->dV = geom->h*geom->h*geom->h;

  // Iterate over domains and check the names and allocate memory for 
  // domain specific information
  REAL max_sigma = 0.;
  REAL* max_sigma_species = mpi_malloc(comm, sizeof(REAL)*species->num_species);
  memfill(max_sigma_species, species->num_species, 0.);
  species->num_all_diffusive = 0;
  unsigned char* all_diffusive = mpi_malloc(comm, sizeof(unsigned char)*species->num_species);
  memset(all_diffusive, 0, sizeof(int)*species->num_species);
  //  memset(species->all_buffers, 0, sizeof(domain_id)*species->num_species);
  species->all_buffers_b = mpi_malloc(comm, sizeof(unsigned char)*species->num_species);
  memset(species->all_buffers_b, 0, sizeof(unsigned char)*species->num_species);
  for (di=0; di<num_domains; di++)
  {
    char name[MAX_SPECIES_NAME];
    char group_name[MAX_SPECIES_NAME];
    char domain_name[MAX_SPECIES_NAME];
    sprintf(name,"domain_name_%zu", di);
    read_h5_attr(comm, file_id, "/", name, domain_name);
    
    // Check domain name
    if (strcmp(geom->domain_names[di], domain_name)!=0)
      mpi_printf_error(comm, "*** ERROR: Expected the domain names to be the "\
		       "same in geometry and model files. %s!=%s\n", 
		       geom->domain_names[di], domain_name);
    
    // Read num diffusive species for this domain
    read_h5_attr(comm, file_id, domain_name, "num_diffusive", &species->num_diffusive[di]);
    species->init[di] = mpi_malloc(comm, sizeof(REAL)*species->num_species);
    read_h5_attr(comm, file_id, domain_name, "init", species->init[di]);
    
    if (species->num_diffusive[di]>0)
    {
    
      species->diffusive[di] = mpi_malloc(comm, sizeof(domain_id)*species->num_diffusive[di]);
      species->sigma[di] = mpi_malloc(comm, sizeof(REAL)*species->num_diffusive[di]);

      read_h5_attr(comm, file_id, domain_name, "diffusive", species->diffusive[di]);
      read_h5_attr(comm, file_id, domain_name, "sigma", species->sigma[di]);
      
      for (si=0; si<species->num_diffusive[di]; si++)
      {
	// Flag the species as a diffusive species
	dsi = species->diffusive[di][si];
	all_diffusive[dsi] = 1;
	max_sigma = fmax(max_sigma, species->sigma[di][si]);
	max_sigma_species[dsi] = fmax(species->sigma[di][si], max_sigma_species[dsi]);
      }
    }
    else
    {
      species->diffusive[di] = NULL;
      species->sigma[di] = NULL;
    }

    // Read num buffers for this domain
    read_h5_attr(comm, file_id, domain_name, "num_buffers", &species->num_buffers[di]);
    
    // Allocate memory 
    species->k_on[di]  = mpi_malloc(comm, sizeof(REAL)*species->num_species);
    memfill(species->k_on[di], species->num_species, 0.);

    species->k_off[di] = mpi_malloc(comm, sizeof(REAL)*species->num_species);
    memfill(species->k_off[di], species->num_species, 0.);

    species->tot[di] = mpi_malloc(comm, sizeof(REAL)*species->num_species);
    memfill(species->tot[di], species->num_species, 0.);

    species->bsp0[di] = mpi_malloc(comm, sizeof(domain_id)*species->num_species);
    memset(species->bsp0[di], 0, sizeof(domain_id)*species->num_species);

    species->bsp1[di] = mpi_malloc(comm, sizeof(domain_id)*species->num_species);
    memset(species->bsp1[di], 0, sizeof(domain_id)*species->num_species);

    // Get all domain specific buffers
    for (bi=0; bi<species->num_buffers[di]; bi++)
    {

      // Local buffer species array
      domain_id bsp[2];

      // Get groupname of buffer
      sprintf(group_name, "/%s/buffer_%zu/", domain_name, bi);

      // Read in buffer attr
      read_h5_attr(comm, file_id, group_name, "k_off", &species->k_off[di][bi]);
      read_h5_attr(comm, file_id, group_name, "k_on", &species->k_on[di][bi]);
      read_h5_attr(comm, file_id, group_name, "tot", &species->tot[di][bi]);
      read_h5_attr(comm, file_id, group_name, "species", bsp);
      species->bsp0[di][bi] = bsp[0];
      species->bsp1[di][bi] = bsp[1];
    	
      // Flag the species as a buffer species
      species->all_buffers_b[bsp[0]] = 1;
    }
  }
  
  // Calculate time step and alpha
  // FIXME: Move to a simulation struct?
  species->dt = arguments->dt > 0 ? DT_SCALE*arguments->dt : (1./6)*geom->h*geom->h/max_sigma;

  // Calculate all diffusive species
  species->num_all_diffusive = 0;
  for (si=0; si<species->num_species; si++)
    species->num_all_diffusive += all_diffusive[si];
  
  species->is_diffusive = mpi_malloc(comm, sizeof(domain_id)*species->num_species);
  memset(species->is_diffusive, 0, sizeof(domain_id)*species->num_species);
  species->all_diffusive = mpi_malloc(comm, sizeof(domain_id)*species->num_all_diffusive);
  species->species_sigma_dt = mpi_malloc(comm, sizeof(REAL)*species->num_all_diffusive);
  species->species_substeps = mpi_malloc(comm, sizeof(domain_id)*species->num_all_diffusive);
  REAL* max_sigma_per_diffusive_species = \
    mpi_malloc(comm, sizeof(REAL)*species->num_all_diffusive);
  memfill(max_sigma_per_diffusive_species, species->num_all_diffusive, 0.);

  dsi=0;
  for (si=0; si<species->num_species; si++)
  {
    if (all_diffusive[si])
    {
      species->all_diffusive[dsi] = si;
      species->species_sigma_dt[dsi] = fabs(max_sigma-max_sigma_species[si])<1e-12 ? \
        species->dt : DT_SCALE*(1./6)*geom->h*geom->h/max_sigma_species[si];
      species->species_substeps[dsi] = fmax(1.,floor(species->species_sigma_dt[dsi]/species->dt));
      species->species_sigma_dt[dsi] = species->dt*species->species_substeps[dsi];
      species->is_diffusive[si] = 1;
      dsi++;
    }
  }
  free(all_diffusive);
  free(max_sigma_species);

  // Calculate the max buffer value for all domains
  species->num_all_buffers = 0;
  for (si=0; si<species->num_species; si++)
    species->num_all_buffers += species->all_buffers_b[si];
  
  // Allocate information about all buffers
  bi=0;
  species->all_buffers = mpi_malloc(comm, sizeof(domain_id)*species->num_all_buffers);
  for (si=0; si<species->num_species; si++)
    if (species->all_buffers_b[si])
      species->all_buffers[bi++] = si;

  // Number of voxels
  for (dim=0; dim<NDIMS; dim++)
  {
    species->N[dim] = geom->N[dim]*geom->subdivisions;
    species->n[dim] = geom->n[dim]*geom->subdivisions;
  }

  // Allocate num_species dependent memory
  species->u1 = mpi_malloc(comm, sizeof(REAL*)*species->num_species);
  species->du = mpi_malloc(comm, sizeof(REAL*)*species->num_species);
  species->species_names = mpi_malloc(comm, sizeof(char*)*species->num_species);
  species->update_species = mpi_malloc(comm, sizeof(domain_id)*species->num_species);
  memset(species->update_species, 0, sizeof(domain_id)*species->num_species);

  // Calculate size of the species arrays
  size_t u_size = 1;
  for (dim=0; dim<NDIMS; dim++)
    u_size *= species->n[dim];

  for (si=0; si<species->num_species; si++)
  {

    char name[MAX_SPECIES_NAME];
    species->species_names[si] = mpi_malloc(comm, sizeof(char)*MAX_SPECIES_NAME);
    sprintf(name,"species_name_%zu", si);
    read_h5_attr(comm, file_id, "/", name, species->species_names[si]);
    
    // Allocate local memory for all species 
  //  species->u1[si] = mpi_malloc(comm, sizeof(REAL)*u_size);
    species->u1[si] = mpi_malloc(comm, sizeof(REAL)*u_size);
    memfill(species->u1[si], u_size, 0.);
  //  species->du[si] = mpi_malloc(comm, sizeof(REAL)*u_size);
    species->du[si] = mpi_malloc(comm, sizeof(REAL)*u_size);
    memfill(species->du[si], u_size, 0.);

  }    

  // Read number of fixed domains
  read_h5_attr(comm, file_id, "/", "num_fixed_domain_species", \
               &species->num_fixed_domains);
  if (species->num_fixed_domains>0)
  {
    species->fixed_domains = mpi_malloc(comm, sizeof(domain_id)*\
                                        species->num_fixed_domains*2);
    read_h5_attr(comm, file_id, "/", "fixed_domain_species", \
                 species->fixed_domains);
    
    // Allocate memory for all species voxels 
    Species_init_fixed_domain_species(species, species->num_fixed_domains, 
                                      species->fixed_domains);
  }
  else
  {
    species->fixed_domains = NULL;
  }

  // Check save species
  species->ind_save_species = NULL;
  species->u1_sheet_save = NULL;
  if (arguments->num_save_species>0)
  {
    
    // Allocate memory
    species->num_save_species = arguments->num_save_species;
    species->ind_save_species = mpi_malloc(comm, sizeof(domain_id)*\
					   arguments->num_save_species);
    for (i=0; i<arguments->num_save_species; i++)
    {
      unsigned char found = 0;
      for (si=0; si<species->num_species; si++)
      {
	if (strcmp(arguments->species[i], species->species_names[si])==0)
	{
	  found = 1;
	  break;
	}
      }

      // Save the species index
      if (found)
      {
	species->ind_save_species[i] = si;
      }

      // If not found raise an error
      else
      {
	char all_species[MAX_FILE_NAME];
	all_species[0] = '\0';
	for (si=0; si<species->num_species; si++)
	{
	  sprintf(all_species, si==0 ? "%s\"%s\"" : "%s, \"%s\"", all_species, \
		  species->species_names[si]);
	}
	mpi_printf_error(comm, "*** ERROR: Expected given save species to be "
			 "present in the model loaded from file. \n\"%s\" cannot "
			 "be found in [%s].\n", arguments->species[i], all_species);
      }
    }

    // Allocate memory to save species in sheets
    species->u1_sheet_save = mpi_malloc(comm, sizeof(REAL)*species->n[X]*species->n[Y]);

  }

  // Calculate alpha values for each diffusive species for each x,y,z direction
  size_t i_offset_geom[NDIMS], i_offset_geom_p[NDIMS], i_offset[NDIMS];
  size_t xi, yi, zi, di_p;
  for (dim=0; dim<NDIMS; dim++)
  {
    
    // Set number of voxels in each direction. The last sheet of voxels in each 
    // direction will not be used, but we use the same number to easy handling
    for (dim1=0; dim1<NDIMS; dim1++)
      n[dim1] = species->n[dim1];
    n[dim] -= 1;
    
    // Allocate memory for diffusive species
    species->alpha[dim] = mpi_malloc(comm, sizeof(REAL*)*species->num_all_diffusive);

    // Iterate over all diffusive species
    for (dsi=0; dsi<species->num_all_diffusive; dsi++)
    {
      
      // Allocate memory for each diffusive species (reuse u_size. Note that this 
      // will allocate slightly more memory than we need, but it will make access 
      // more standardized with same offsets and so on.)
      //species->alpha[dim][dsi] = mpi_malloc(comm, sizeof(REAL)*u_size);
      species->alpha[dim][dsi] = mpi_malloc(comm, sizeof(REAL)*u_size);
      memfill(species->alpha[dim][dsi], u_size, 0.);

      // Get species index
      si = species->all_diffusive[dsi];
      assert(si<species->num_all_diffusive);

      // Iterate over all voxels and fill with alpha values
      // NOTE: If dim == X then n[X] == species->n[X]-1
      for (xi=0; xi<n[X]; xi++)
      {

	// Offsets into geom array
	i_offset_geom[X] = (xi/geom->subdivisions)*geom->n[Z]*geom->n[Y];

	// If creating values for the X direction we grab the next Y value 
	if (dim==X)
	  i_offset_geom_p[X] = ((xi+1)/geom->subdivisions)*geom->n[Z]*geom->n[Y];
	else
	  i_offset_geom_p[X] = i_offset_geom[X];
	
	// Offset into alpha array
	i_offset[X] = xi*species->n[Z]*species->n[Y];
	
	// NOTE: If dim == Y then n[Y] == species->n[Y]-1
	for (yi=0; yi<n[Y]; yi++)
	{
	
	  // Offsets into geom array
	  i_offset_geom[Y] = (yi/geom->subdivisions)*geom->n[Z];
	  
	  // If creating values for the Y direction we grab the next Y value 
	  if (dim==Y)
	    i_offset_geom_p[Y] = ((yi+1)/geom->subdivisions)*geom->n[Z];
	  else
	    i_offset_geom_p[Y] = i_offset_geom[Y];
	  
	  // Offset into alpha array
	  i_offset[Y] = yi*species->n[Z];
	  
	  // NOTE: If dim == Z then n[Z] == species->n[Z]-1
	  for (zi=0; zi<n[Z]; zi++)
	  {
	    i_offset_geom[Z] = zi/geom->subdivisions;

	    // If creating values for the Z direction we grab the next Z value 
	    if (dim==Z)
	      i_offset_geom_p[Z] = ((zi+1)/geom->subdivisions);
	    else
	      i_offset_geom_p[Z] = (zi/geom->subdivisions);

	    // Get domain index and domain index for the next voxel in the dim direction
	    di = geom->domains[i_offset_geom[X]+i_offset_geom[Y]+i_offset_geom[Z]];
	    di_p = geom->domains[i_offset_geom_p[X]+i_offset_geom_p[Y]+i_offset_geom_p[Z]];
	    
	    // If the same domain we set alpha based on diffusion constant
	    if (di_p == di && dsi<species->num_diffusive[di])
            {
	      // dt*D/h^2*num_substeps
	      species->alpha[dim][dsi][i_offset[X]+i_offset[Y]+zi] = \
		species->dt*species->sigma[di][dsi]/(geom->h*geom->h)*\
		species->species_substeps[dsi];
            }

            // If the two domains are connected
	    else if (geom->domain_connections[di_p*geom->num_domains+di] \
                     && dsi<species->num_diffusive[di])
            {

              // If one of the domains has zero diffusion
              if (dsi>=species->num_diffusive[di] || species->sigma[di][dsi]==0 || \
                  dsi>=species->num_diffusive[di_p] || species->sigma[di_p][dsi]==0)
              {
                species->alpha[dim][dsi][i_offset[X]+i_offset[Y]+zi] = 0.0;
              }
              else
              {
                REAL avg_sigma = (species->sigma[di][dsi]+species->sigma[di_p][dsi])/2;
                species->alpha[dim][dsi][i_offset[X]+i_offset[Y]+zi] =  \
                  species->dt*avg_sigma/(geom->h*geom->h)* species->species_substeps[dsi];
              }
            }

	    // If no diffusive species in domain or we shift domain across boundary
            else
            {
	      species->alpha[dim][dsi][i_offset[X]+i_offset[Y]+zi] = 0.0;
            }
	  }
	}
      }
    }
  }

  // Allocate ghost values
  size_t oi, ii, di_g;
  unsigned int domain_offsets, domain_offset_o, odim, idim;
  unsigned int domain_outer_offsets;
  unsigned int domain_inner_offsets;
  for (dim2=0; dim2<NDIMS*2; dim2++)
  {
    // X, Y, Z dimension
    size_t loc_dim = dim2/2;
    unsigned char left_right = dim2 % 2;

    // YZ sheet (every thing is contigous)
    if (loc_dim == X)
    {

      // Sheet offset
      if (left_right==0)
      {
	domain_offsets = 0;
	species->u_offsets[dim2] = 0;
	species->u_inner_sheet_offsets[dim2] = species->n[Y]*species->n[Z];
      }
      else
      {
	domain_offsets = (geom->n[X]-1)*geom->n[Y]*geom->n[Z];
	species->u_offsets[dim2] = (species->n[X]-1)*species->n[Y]*species->n[Z];
	species->u_inner_sheet_offsets[dim2] = (species->n[X]-2)*species->n[Y]*species->n[Z];
      }

      // Each outer iteration is a stave
      domain_outer_offsets = geom->n[Z];
      species->u_outer_offsets[dim2] = species->n[Z];

      // Contigous!
      domain_inner_offsets = 1;
      species->u_inner_offsets[dim2] = 1;
    }

    // XZ sheet
    else if (loc_dim == Y)
    {
      // Sheet offset
      if (left_right==0)
      {
	domain_offsets = 0;
	species->u_offsets[dim2] = 0;
	species->u_inner_sheet_offsets[dim2] = species->n[Z];
      }
      else
      {
	domain_offsets = (geom->n[Y]-1)*geom->n[Z];
	species->u_offsets[dim2] = (species->n[Y]-1)*species->n[Z];
	species->u_inner_sheet_offsets[dim2] = (species->n[Y]-2)*species->n[Z];
      }
      
      // Each outer iteration is a sheet
      domain_outer_offsets = geom->n[Y]*geom->n[Z];
      species->u_outer_offsets[dim2] = species->n[Y]*species->n[Z];

      // Contigous!
      domain_inner_offsets = 1;
      species->u_inner_offsets[dim2] = 1;
    }

    // YZ sheet
    else
    {

      // Sheet offset
      if (left_right==0)
      {
	domain_offsets = 0;
	species->u_offsets[dim2] = 0;
	species->u_inner_sheet_offsets[dim2] = 1;
      }
      else
      {
	domain_offsets = geom->n[Z]-1;
	species->u_offsets[dim2] = species->n[Z]-1;
	species->u_inner_sheet_offsets[dim2] = species->n[Z]-2;
      }
      
      // Each outer iteration is a sheet
      domain_outer_offsets = geom->n[Y]*geom->n[Z];
      species->u_outer_offsets[dim2] = species->n[Y]*species->n[Z];

      // Each inner iteration is a stave
      domain_inner_offsets = geom->n[Z];
      species->u_inner_offsets[dim2] = species->n[Z];
    }

    // Allocate ghost_alpha memories per species
    species->ghost_alpha[dim2] = mpi_malloc(comm, sizeof(REAL*)*species->num_all_diffusive);

    // Reimplementation of ghost update
    species->ghost_values_send[dim2] = mpi_malloc(comm, sizeof(REAL*)* \
                                                  species->num_all_diffusive);
    species->ghost_values_receive[dim2] = mpi_malloc(comm, sizeof(REAL*)* \
                                                     species->num_all_diffusive);
    species->size_ghost_values[dim2] = 0;
    for (dsi=0; dsi<species->num_all_diffusive; dsi++)
    {
      species->ghost_values_receive[dim2][dsi] = NULL;
      species->ghost_values_send[dim2][dsi] = NULL;
      species->ghost_alpha[dim2][dsi] = NULL;
    }

    // If neighbor
    if (geom->mpi_info.neighbors[dim2]!=MPI_PROC_NULL)
    {
      
      // Allocate memory 
      size_t size_ghost_values = 1;
      for (dim=0; dim<NDIMS; dim++)
	if (loc_dim!=dim)
	  size_ghost_values*=species->n[dim];

      species->size_ghost_values[dim2] = size_ghost_values;

      for (dsi=0; dsi<species->num_all_diffusive; dsi++)
      {
        species->ghost_values_send[dim2][dsi] = mpi_malloc(comm, sizeof(REAL)*\
                                                           size_ghost_values);
        species->ghost_values_receive[dim2][dsi] = mpi_malloc(comm, sizeof(REAL)* \
                                                              size_ghost_values);
        memfill(species->ghost_values_send[dim2][dsi], size_ghost_values, -1.0);
        memfill(species->ghost_values_receive[dim2][dsi], size_ghost_values, -1.0);
      }
      
      // Allocate ghosted alpha values
      for (dsi=0; dsi<species->num_all_diffusive; dsi++)
	species->ghost_alpha[dim2][dsi] = mpi_malloc(comm, sizeof(REAL)*size_ghost_values);
      
      // Assume same alpha (diffusion) for all ghosted alpha values for now. 
      // If we have a boundary at a particular ghost boundary we overwrite it later.
      
      // Get u offsets (u offset is the same as alpha offset!)
      // If left right == 1 we need the inner sheet for the alpha offset
      //alpha_offset = left_right==0 ? species->u_offsets[dim2] : 
      //  species->u_inner_sheet_offsets[dim2];
      //alpha_inner_offset = species->u_inner_offsets[dim2];
      //alpha_outer_offset = species->u_outer_offsets[dim2];

      // Get the dimension of the outer and inner loops
      odim = loc_dim != X ? X : Y;
      idim = loc_dim != Z ? Z : Y;

      // Update each diffusive species
      for (dsi=0; dsi<species->num_all_diffusive; dsi++)
      {
	
	// Outer loop
	for (oi=0; oi<species->n[odim]; oi++)
	{
	  
	  // Outer offset
	  domain_offset_o = domain_offsets+domain_outer_offsets*(oi/geom->subdivisions);
	  //alpha_offset_i = alpha_offset+alpha_outer_offset*oi;

	  // Inner loop
	  for (ii=0; ii<species->n[idim]; ii++)
	  {
	    
	    // Get domain id and ghost domain id
	    di = geom->domains[domain_offset_o+domain_inner_offsets*(ii/geom->subdivisions)];
            di_g = geom->ghost_domains[dim2][geom->n[idim]*(oi/geom->subdivisions)+ \
                                             ii/geom->subdivisions];

	    // If same domain and there are diffusive species in the domain we 
            // assign the same alpha as the nearest inner alpha_value
            if(di == di_g && dsi<species->num_diffusive[di])
            {
	      /*
              species->ghost_alpha[dim2][dsi][species->n[idim]*oi+ii] =	\
		species->alpha[loc_dim][dsi][alpha_offset_i+alpha_inner_offset*ii];
              */
              
              species->ghost_alpha[dim2][dsi][species->n[idim]*oi+ii] =	\
                species->dt*species->sigma[di][dsi]/(geom->h*geom->h)*  \
		species->species_substeps[dsi];
              
#if DEBUG
              printf("%d|%s[%zu][%zu,%zu] alpha value: %.4f|%.4f\n", 
	             geom->mpi_info.rank, (loc_dim==X)?"X":(loc_dim==Y? "Y":"Z"), 
	             dim2%2, oi, ii, 
                     species->ghost_alpha[dim2][dsi][species->n[idim]*oi+ii],
                     species->dt*species->sigma[di][dsi]/(geom->h*geom->h)*    \
                     species->species_substeps[dsi]);
#endif	    
            }

            // If boundary is between two connected domains
            else if (geom->domain_connections[di_g*geom->num_domains+di] && 
                     dsi<species->num_diffusive[di])
            {

              // If one of the domains has zero diffusion
              if (species->sigma[di][dsi]==0 || species->sigma[di_g][dsi]==0)
              {
                species->ghost_alpha[dim2][dsi][species->n[idim]*oi+ii] = 0.0;
              }
              else
              {
                REAL avg_sigma = (species->sigma[di][dsi]+species->sigma[di_g][dsi])/2;
                species->ghost_alpha[dim2][dsi][species->n[idim]*oi+ii] =  \
                  species->dt*avg_sigma/(geom->h*geom->h)* species->species_substeps[dsi];
#if DEBUG
                printf("Connected ghosted %s->%s avg sigma: %f\n", geom->domain_names[di], 
                       geom->domain_names[di_g], avg_sigma);
#endif	    
              }
            }

	    // If domain is different to the ghosted domain we nullify the alpha or there 
            // are no diffusive species in the domain
	    else
	    {
#if DEBUG
	      printf("%d|%s[%zu][%zu,%zu] (ghost domain!)\n", 
		     geom->mpi_info.rank, (loc_dim==X)?"X":(loc_dim==Y? "Y":"Z"), 
		     dim2%2, oi, ii);
	      printf("%d|We have a ghost boundary!\n", geom->mpi_info.rank);
#endif	    
	      species->ghost_alpha[dim2][dsi][species->n[idim]*oi+ii] = 0.0;
	    }
	  }
	}
      }
    }
    else
    {
    }
  }

  // Init voxel indices for all boundaries
  Species_init_boundary_voxels(species);

  // FIXME: In future releases we need to make this more flexible...
  // Hard code everything about ryr and serca
  species->boundary_fluxes = BoundaryFluxes_construct(species, file_id, arguments);
  
  H5Fclose(file_id);
  // Init stochastic event timestepping
  species->stochastic_substep = fmax(1., floor(arguments->dt_update_ryrs/species->dt));

  // Init reaction timestepping
  species->reaction_substep = fmax(1., floor(arguments->dt_update_react/species->dt));

  // If not given set default z coordinate and z indices for saving sheets
  if (arguments->num_save_species && arguments->num_z_points)
  {

    // If no z point was given we prep it with the center coordinate
    if (arguments->z_points == NULL)
    {
      arguments->z_points = mpi_malloc(comm, sizeof(REAL));
      arguments->z_points[0] = (species->N[Z]/2-geom->subdivisions/2)*geom->h;
    }

    species->num_sheets_per_species = arguments->num_z_points;
    species->z_indices = mpi_malloc(comm, sizeof(int)*arguments->num_z_points);
    species->z_global_indices = mpi_malloc(comm, sizeof(unsigned int)*arguments->num_z_points);
    species->z_coords  = mpi_malloc(comm, sizeof(REAL)*arguments->num_z_points);
      
    for (i=0; i<arguments->num_z_points; i++)
    {
      if (arguments->z_points[i]<=0 || arguments->z_points[i]>=species->N[Z]*geom->h)
        mpi_printf_error(comm, "*** ERROR: Expected given z coordinate for save "
                         "species to be within: (0,%f), got: %f.\n", 
                         species->N[Z]*geom->h, arguments->z_points[i]);

      // Get global index
      species->z_global_indices[i] = floor(arguments->z_points[i]/geom->h);
        
      // Check if it is contained within this process
      if (geom->offsets[Z]*geom->subdivisions<=species->z_global_indices[i] && 
          species->z_global_indices[i]<(geom->offsets[Z]+geom->n[Z])*geom->subdivisions)
      {
        // Local z-index
        species->z_indices[i] = species->z_global_indices[i]-geom->offsets[Z]*geom->subdivisions;  
      }
        
      else
      {
        species->z_indices[i] = -1;
      }

      // Copy value
      species->z_coords[i] = arguments->z_points[i];
        
    }
  }

  return species;

}
//-----------------------------------------------------------------------------
void Species_init_values(Species_t* species)
{
  size_t di, si, xi, yi, zi, dim, xi_offset_geom;
  size_t xi_offset, yi_offset_geom, yi_offset, zi_geom;
  Geometry_t* geom = species->geom;

  size_t u_size = 1;
  for (dim=0; dim<NDIMS; dim++)
    u_size *= species->n[dim];

  for (si=0; si<species->num_species; si++)
  {
    // Initialize values
    for (xi=0; xi<species->n[X]; xi++)
    {
      xi_offset_geom = (xi/geom->subdivisions)*geom->n[Y]*geom->n[Z];
      xi_offset = xi*species->n[Y]*species->n[Z];
      for (yi=0; yi<species->n[Y]; yi++)
      {
	yi_offset_geom = (yi/geom->subdivisions)*geom->n[Z];
	yi_offset = yi*species->n[Z];
	for (zi=0; zi<species->n[Z]; zi++)
	{
	  zi_geom = zi/geom->subdivisions;
	  di = geom->domains[xi_offset_geom+yi_offset_geom+zi_geom];
	  assert(di<geom->num_domains);
	  species->local_domain_num[di] += 1;
	  species->u1[si][xi_offset+yi_offset+zi] = species->init[di][si];
	}
      }
    }
  }

  // Sanity check!
  for (di=0; di<geom->num_domains; di++)
  {
    species->local_domain_num[di]/=species->num_species;
    if(fabs(species->local_domain_num[di]*species->dV-geom->local_domain_num[di]*geom->dV)>1e-16)
      printf("*** ERROR: [%d] The number of species domains is not the same: %f!=%f.\n", 
	     geom->mpi_info.rank, species->local_domain_num[di]*species->dV,
	     geom->local_domain_num[di]*geom->dV);
  }
}
//-----------------------------------------------------------------------------
void Species_init_boundary_voxels(Species_t* species)
{
  unsigned int bi, dim2, dim, left_right, odim, idim, oi, ii, from_ind, to_ind;
  unsigned int o_ind, i_ind, boundary_offset, boundary_id, subdivisions;
  unsigned short* loc_boundary;
  unsigned int* loc_boundary_voxels;
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;
  
  // Allocate memory
  species->num_boundary_voxels = mpi_malloc(comm, sizeof(unsigned int)*geom->num_boundaries);
  species->boundary_voxels = mpi_malloc(comm, sizeof(unsigned int*)*geom->num_boundaries);

  // Iterate ovet all boundaries
  for (boundary_id=0; boundary_id<geom->num_boundaries; boundary_id++)
  {

    // Initiate voxel memory
    subdivisions = geom->boundary_types[boundary_id] == discrete ? 1 : geom->subdivisions;
    species->num_boundary_voxels[boundary_id] = subdivisions*subdivisions*\
      geom->local_boundary_size[boundary_id];
    species->boundary_voxels[boundary_id] = mpi_malloc(comm, sizeof(unsigned int)*\
                                        species->num_boundary_voxels[boundary_id]*2*NDIMS);

    // Iterate over geometry boundaries and initiate flux voxels
    for (bi=0; bi<geom->local_boundary_size[boundary_id]; bi++)
    {
      
      // Get local boundary offset
      loc_boundary = &geom->boundaries[boundary_id][bi*2*NDIMS];
      boundary_offset = bi*2*NDIMS*subdivisions*subdivisions;
    
      // Get direction of boundary
      dim2 = Geometry_get_boundary_dir(geom, boundary_id, bi);
      dim = dim2/2;
      left_right = dim2%2;
      odim = dim!=X ? X : Y;
      idim = dim!=Z ? Z : Y;

      // Get from and to ind in the direction of flux. This index should be the same 
      // for the inner and outer directions
    
      // Logic for from_ind. (Opposite logic for to_ind)
      // 1) offset basic voxel dim with subdivisions. 
      // 2) If voxel is left to right (left_right=1) we need to find the last 
      //    voxel row in this direction, hence subdivision-1. if right to left 
      //    (left_right=0) we add 0.
      //   
      from_ind = loc_boundary[dim]*geom->subdivisions + (left_right==0 ? \
                                                         geom->subdivisions-1 : 0);
      to_ind =   loc_boundary[dim+NDIMS]*geom->subdivisions + (left_right==0 ? \
                                                               0 : geom->subdivisions-1);
      
      /*
      printf("Boundary[%d|%d]: dim:[%d-%d][%d,%d](%d->%d)\n", boundary_id, 
             bi, dim, left_right, odim, idim, from_ind, to_ind);
      */
      // Iterate over the outer dimension
      for (oi=0; oi<subdivisions; oi++)
      {
        
        // Find outer index
        o_ind = geom->subdivisions*loc_boundary[odim]+oi;
        
        // If boundary type is discrete we need to offset the index
        if (geom->boundary_types[boundary_id] == discrete)
          o_ind += geom->subdivisions/2;
    
        // Iterate over the inner dimension
        for (ii=0; ii<subdivisions; ii++)
        {
          
          // Get local flux voxel
          loc_boundary_voxels = &species->boundary_voxels[boundary_id][\
                                        boundary_offset+(oi*subdivisions+ii)*2*NDIMS];

          // Find inner index
          i_ind = geom->subdivisions*loc_boundary[idim]+ii;
          
          // If boundary type is discrete we need to offset the index
          if (geom->boundary_types[boundary_id] == discrete)
            i_ind += geom->subdivisions/2;
    
          // Store the from and to voxels
          loc_boundary_voxels[dim]  = from_ind;
          loc_boundary_voxels[odim] = o_ind;
          loc_boundary_voxels[idim] = i_ind;
    
          loc_boundary_voxels[dim+NDIMS]  = to_ind;
          loc_boundary_voxels[odim+NDIMS] = o_ind;
          loc_boundary_voxels[idim+NDIMS] = i_ind;
          
        }
      }
    }
  }
}
//-----------------------------------------------------------------------------
void Species_apply_du(Species_t* species)
{
  unsigned int si, xi, yi, zi, dsi;
  unsigned int i_offset[NDIMS];

  // Needed for vectorization
  size_t nz = species->n[Z];
  
  for (si=0; si<species->num_species; si++)
  {

    // If substeping 
    if (!species->update_species[si])
      continue;

    // Reset update species
    species->update_species[si] = 0;
    
    // Zero out du for all fixed domain species
    for (dsi=0; dsi<species->num_fixed_domain_species[si]; dsi++)
      species->du[si][species->fixed_domain_species[si][dsi]] = 0.;

    for (xi=0; xi<species->n[X]; xi++)
    {

      // Compute the X offsets 
      i_offset[X] = xi*species->n[Y]*species->n[Z];

      // Iterate over the interior Y indices
      for (yi=0; yi<species->n[Y]; yi++)
      {

        // Compute the Y offsets 
        i_offset[Y] = yi*species->n[Z];

//#pragma ivdep
//#pragma simd 
//#pragma prefetch
        // Make the increment
	for (zi=0; zi<nz; zi++)
	{
          species->u1[si][i_offset[X]+i_offset[Y]+zi] += \
            species->du[si][i_offset[X]+i_offset[Y]+zi];
          //printf("du[%d,%d,%d]=%e\n", xi,yi,zi, species->du[si][i_offset[X]+i_offset[Y]+zi]);
          species->du[si][i_offset[X]+i_offset[Y]+zi] = 0.;
        }
      }
    }
  }
}
//-----------------------------------------------------------------------------
void Species_step_diffusion(Species_t* species, size_t time_ind)
{
  Geometry_t* geom = species->geom;
  unsigned int si, dsi, xi, yi, zi, dim, dim2, odim, idim;
  unsigned int i_offset[NDIMS], i_offset_p[NDIMS], i_offset_m[NDIMS];
  unsigned int offset, offset_m, offset_p;
  REAL* restrict u1;
  REAL* restrict du;
  REAL* restrict alphaX;
  REAL* restrict alphaY;
  REAL* restrict alphaZ;
  REAL* restrict alpha;
  REAL* alpha_o;
  REAL* alpha_i;
  REAL* ghost_alpha;
  REAL u;
 #ifdef __PAPI__
        int Events[]={/*PAPI_DP_OPS,*/PAPI_L1_DCM,PAPI_L2_DCM};//PAPI_TLB_DM};//,*/PAPI_LD_INS,PAPI_SR_INS};
        int NUM_EVENTS = sizeof(Events)/sizeof(Events[0]);
        long long res_papi[NUM_EVENTS];
        char EventName[128];
        int num_hwcntrs = 0;
        int EventSet = PAPI_NULL;
        int retval;

        float real_time, proc_time, mflops;
        long long flpins;
	MPI_Comm comm;
	int tt;
	comm = geom->mpi_info.comm;
if(time_ind==1)
{
        retval = PAPI_library_init(PAPI_VER_CURRENT);
        retval = PAPI_create_eventset(&EventSet);
        if(PAPI_add_events(EventSet, Events, NUM_EVENTS)!=PAPI_OK)
                printf("PAPI_add_events failed!\n");
        for( tt=0;tt<NUM_EVENTS;tt++)
                res_papi[tt] = 0;
} 
#endif

  
  // Update ghost values
  Species_update_ghost(species, time_ind);

  // Needed for vectorization
  size_t nz = species->n[Z];

  // Iterate over the interior X indices
  push_time(DIFF);

#ifdef __PAPI__
      if(time_ind==1)
      {
                if(PAPI_start(EventSet)!=PAPI_OK)
                        mpi_printf0(comm,"PAPI_read_counters failed!\n");
		if(PAPI_stop(EventSet,res_papi)!=PAPI_OK)
	                mpi_printf0(comm,"PAPI_accum_counters failed! \n");
		
      }
#endif

  int x_chunk_size,y_chunk_size,z_chunk_size;
  int xc,yc,zc;
  int xm,ym,zm;
  //REAL DU;

  x_chunk_size = 8;
  y_chunk_size = 8;
  z_chunk_size = 64;

  unsigned int n_X,n_Y,n_Z;
  unsigned int offset_X,offset_Y;
  unsigned int xi_offset_X,yi_offset_Y;
 // unsigned int indexID;
  n_X = species->n[X];
  n_Y = species->n[Y];
  n_Z = species->n[Z];
  offset_X = n_Y*n_Z;
  offset_Y = n_Z;
#ifdef __PAPI__
      if(time_ind==1)
      {
                if(PAPI_start(EventSet)!=PAPI_OK)
                        mpi_printf0(comm,"PAPI_read_counters failed!\n");
      }
#endif

for (dsi=0; dsi<species->num_all_diffusive; dsi++)
            {
      
                    // If substeping 
                     if (time_ind % species->species_substeps[dsi] != 0)
                             continue;
      
                      // Get species index
                      si = species->all_diffusive[dsi];
                      u1 = species->u1[si];
                      du = species->du[si];
                      species->update_species[si] = 1;

		      //calculate alphaX,alphaY,alphaZ
		      alphaX = species->alpha[X][dsi];	
		      alphaY = species->alpha[Y][dsi];	
		      alphaZ = species->alpha[Z][dsi];	
/*for(zc=1;zc<species->n[Z]-1;zc+=z_chunk_size)
    for(xc=1;xc<species->n[X]-1;xc+=x_chunk_size)
	for(yc=1;yc<species->n[Y]-1;yc+=y_chunk_size)
		//for(zc=1;zc<species->n[Z]-1;zc+=z_chunk_size)
 {*/
  //xm = (xc+x_chunk_size<species->n[X]-1)?xc+x_chunk_size:species->n[X]-1;
  //#pragma omp parallel 
  //{
  //#pragma omp for
  for (xi=1; xi<species->n[X]-1; xi++)
  //for (xi=xc; xi<xm; xi++)
  {

    // Compute the X offsets 
    xi_offset_X = xi*offset_X;

    
    //ym = (yc+y_chunk_size<species->n[Y]-1)?yc+y_chunk_size:species->n[Y]-1;
    // Iterate over the interior Y indices
    for (yi=1; yi<species->n[Y]-1; yi++)
    //for (yi=yc; yi<ym; yi++)
    {

      // Compute the Y offsets 
 	yi_offset_Y = yi*offset_Y;
        //zm = (zc+z_chunk_size<species->n[Z]-1)?zc+z_chunk_size:species->n[Z]-1;

	
	// Iterate over the interior Z indices
//#pragma simd 
//#pragma vector aligned
//#pragma prefetch
//#pragma vector nontemporal(du)
//#pragma ivdep
//#pragma novector
	/*unsigned int index_base;
	unsigned int loopNUM,loopMax;
	loopNUM = (nz-2)/4;
	loopMax = 4*loopNUM+1;
	index_base = xi_offset_X + yi_offset_Y;
	__m256d DU,DU_temp,U,U1,UMU1,AL,AU,AU1;
	double *ddu = (double *)_mm_malloc(64*sizeof(double),64);
        __declspec (align(64,0)) double arrayIn[10] = {0.1,0.2,0.3,0.4,0.5,0.3,0.1,0.2,0.2,0.3};*/
        //for(zi=1;zi<loopMax;zi+=4)
	for (zi=1; zi<nz-1; zi++)
	//for (zi=zc; zi<zm; zi++)
	{
	    
	  // Fetch the present value
	  unsigned int indexID = xi_offset_X+yi_offset_Y+zi;
	  u = u1[indexID];

	  // NOTE: alpha default offset is always given in the pluss direction
	  // X direction
	  REAL DU = alphaX[indexID-offset_X]*(u1[indexID-offset_X]-u) + alphaX[indexID]*(u1[indexID+offset_X]-u);


	  // Y direction
	  DU += alphaY[indexID-offset_Y]*(u1[indexID-offset_Y]-u) + alphaY[indexID]*(u1[indexID+offset_Y]-u);

          // Z direction
	  DU += alphaZ[indexID-1]*(u1[indexID-1]-u) + alphaZ[indexID]*(u1[indexID+1]-u);
	  du[indexID] = DU;
	  /*U = _mm256_load_pd(u1+index_base+zi);
	  //X direction
	  U1 = _mm256_load_pd(u1+index_base+zi-offset_X);
	  UMU1 = _mm256_sub_pd(U1,U);
	  AL = _mm256_load_pd(alphaX+index_base+zi-offset_X);
	  AU = _mm256_mul_pd(AL,UMU1);
	  
	  U1 = _mm256_load_pd(u1+index_base+zi+offset_X);
	  UMU1 = _mm256_sub_pd(U1,U);
	  AL = _mm256_load_pd(alphaX+index_base+zi);
	  AU1 = _mm256_mul_pd(AL,UMU1);
	  
	  DU = _mm256_add_pd(AU,AU1);
	  //Y direction
	  U1 = _mm256_load_pd(u1+index_base+zi-offset_Y);
          UMU1 = _mm256_sub_pd(U1,U);
          AL = _mm256_load_pd(alphaY+index_base+zi-offset_Y);
          AU = _mm256_mul_pd(AL,UMU1);
          
          U1 = _mm256_load_pd(u1+index_base+zi+offset_Y);
          UMU1 = _mm256_sub_pd(U1,U);
          AL = _mm256_load_pd(alphaY+index_base+zi);
          AU1 = _mm256_mul_pd(AL,UMU1);
                  
          DU_temp = _mm256_add_pd(AU,AU1);
	  DU = _mm256_add_pd(DU,DU_temp);
	  //Z direction
	  U1 = _mm256_load_pd(u1+index_base+zi-1);
          UMU1 = _mm256_sub_pd(U1,U);
          AL = _mm256_load_pd(alphaZ+index_base+zi-1);
          AU = _mm256_mul_pd(AL,UMU1);

          U1 = _mm256_load_pd(u1+index_base+zi+1);
          UMU1 = _mm256_sub_pd(U1,U);
          AL = _mm256_load_pd(alphaZ+index_base+zi);
          AU1 = _mm256_mul_pd(AL,UMU1);

          DU_temp = _mm256_add_pd(AU,AU1);
          DU = _mm256_add_pd(DU,DU_temp);*/
	
 	  //_mm256_store_pd(du+index_base+zi,DU);
 	 // DU = _mm256_load_pd(arrayIn);
 	 // _mm256_store_pd(ddu,DU);

	}

	//DU = _mm256_load_pd(arrayIn);
        //_mm256_store_pd(ddu,DU);

	//_mm_free(ddu);
	/*for (zi=loopMax; zi<nz-1; zi++)
	{
	  unsigned int indexID = xi_offset_X+yi_offset_Y+zi;
          u = u1[indexID];
	  REAL DU = alphaX[indexID-offset_X]*(u1[indexID-offset_X]-u) + alphaX[indexID]*(u1[indexID+offset_X]-u);
	  DU += alphaY[indexID-offset_Y]*(u1[indexID-offset_Y]-u) + alphaY[indexID]*(u1[indexID+offset_Y]-u);
	  DU += alphaZ[indexID-1]*(u1[indexID-1]-u) + alphaZ[indexID]*(u1[indexID+1]-u);
          du[indexID] = DU;
	}*/
     }
  }
// }//pragma omp parallel
}
 #ifdef __PAPI__
        if(time_ind==1)
        {
                if(PAPI_stop(EventSet,res_papi)!=PAPI_OK)
                       mpi_printf0(comm,"PAPI_accum_counters failed!\n");
                               for( tt=0;tt<NUM_EVENTS;tt++)
                                       {
                                               PAPI_event_code_to_name(Events[tt],EventName);
                                               mpi_printf0(comm,"PAPI Event name: %s, value: %lld \n",EventName, res_papi[tt]);
                                        }
				//mpi_printf0(comm,"opt1:move the diffusive loop out!\n");
				//mpi_printf0(comm,"opt2:replace du with a temp variable!\n");
				//mpi_printf0(comm,"opt3:replace offset array to variable!\n");
				//mpi_printf0(comm,"opt4:block!\n");
				//mpi_printf0(comm,"opt5:prefetch!\n");
				//mpi_printf0(comm,"opt6:openmp!\n");
				//mpi_printf0(comm,"opt7:openmp affinity!\n");
				//mpi_printf0(comm,"opt10:compiler vec!\n");
				mpi_printf0(comm,"opt4-4:compiler vec!\n");

        }
 #endif

  pop_time(DIFF);

  // Declarations
  size_t oi, ii, u_inner_offset, u_outer_offset;
  size_t u_offset_0, u_offset, u_offset_m, u_offset_p;
  size_t u_offset_o, u_offset_o_m, u_offset_o_p;
  size_t buffer_offset;
  size_t u_inner_sheet_offset, u_inner_sheet_offset_o, alpha_offset;
  size_t inner_limit_0, inner_limit_1, outer_limit_0, outer_limit_1;
  unsigned char ghosted;

  // Iterate over all end sheets
  push_time(BORDERS);
  for (dim2=0; dim2<NDIMS*2; dim2++)
  {
    
    // Check if sheet is ghosted
    ghosted = geom->mpi_info.neighbors[dim2]!=MPI_PROC_NULL;

    // X, Y, Z dimension
    dim = dim2/2;
    
    // Get the dimension of the outer and inner loops of the sheet
    odim = dim != X ? X : Y;
    idim = dim != Z ? Z : Y;
    
    outer_limit_0 = dim == X ? 0 : 1;
    outer_limit_1 = dim == X ? species->n[odim] : species->n[odim]-1;
    inner_limit_0 = dim != Z ? 0 : 1;
    inner_limit_1 = dim != Z ? species->n[idim] : species->n[idim]-1;

    // Get u offsets
    u_offset_0 = species->u_offsets[dim2];
    u_inner_offset = species->u_inner_offsets[dim2];
    u_outer_offset = species->u_outer_offsets[dim2];
    u_inner_sheet_offset = species->u_inner_sheet_offsets[dim2];

    // Update each diffusive species
    for (dsi=0; dsi<species->num_all_diffusive; dsi++)
    {

      // If substeping 
      if (time_ind % species->species_substeps[dsi] != 0)
        continue;

      // Get corresponding species ind
      si = species->all_diffusive[dsi];
      species->update_species[si] = 1;
      
      // Get local species
      u1 = species->u1[si];
      du = species->du[si];

      // Get alpha arrays
      alpha = species->alpha[dim][dsi];
      alpha_o = species->alpha[odim][dsi];
      alpha_i = species->alpha[idim][dsi];
      ghost_alpha = species->ghost_alpha[dim2][dsi];
	
      // Outer loop
      for (oi=outer_limit_0; oi<outer_limit_1; oi++)
      {
        
	// Offsets
	buffer_offset = species->n[idim]*oi;
	u_offset_o   = u_offset_0+u_outer_offset* oi;
	u_offset_o_m = u_offset_0+u_outer_offset*(oi-1);
	u_offset_o_p = u_offset_0+u_outer_offset*(oi+1);
	u_inner_sheet_offset_o = u_inner_sheet_offset + u_outer_offset*oi;
	
	// Offset for alpha is dependent on what direction (left/right) we are going
	alpha_offset = (dim2 % 2 == 0) ? u_offset_o : u_inner_sheet_offset_o;

	// Inner loop
	for (ii=inner_limit_0; ii<inner_limit_1; ii++)
	{
    	    
	  // Center values
	  u_offset = u_offset_o+u_inner_offset*ii;
	  u = u1[u_offset];

	  // Negative ghost direction
	  du[u_offset] += alpha[alpha_offset+u_inner_offset*ii]* \
	    (u1[u_inner_sheet_offset_o+u_inner_offset*ii]-u);

	  // Outer direction
          u_offset_m = u_offset_o_m+u_inner_offset*ii;
          u_offset_p = u_offset_o_p+u_inner_offset*ii;
          du[u_offset] += oi != 0 ? alpha_o[u_offset_m]*(u1[u_offset_m]-u) : 0.0;
          du[u_offset] += oi != species->n[odim]-1 ? alpha_o[u_offset]*(u1[u_offset_p]-u) : 0.0;
          
	  // Inner direction
          u_offset_m = u_offset_o+u_inner_offset*(ii-1);
          u_offset_p = u_offset_o+u_inner_offset*(ii+1);
          du[u_offset] += ii != 0 ? alpha_i[u_offset_m]*(u1[u_offset_m]-u) : 0.0;
          du[u_offset] += ii != species->n[idim]-1 ? alpha_i[u_offset]*(u1[u_offset_p]-u) : 0.0;
          
          // Ghost direction
	  du[u_offset] += ghosted ? ghost_alpha[species->n[idim]*oi+ii]* \
	    (species->ghost_values_receive[dim2][dsi][buffer_offset+ii]-u) : 0.0;

#if DEBUG
	  if (fabs(alpha[alpha_offset+u_inner_offset*ii]*\
		   (u1[u_inner_sheet_offset_o+u_inner_offset*ii]-u))>1e-6)
	  {
	    printf("(%zu)%d|%s[%d][%zu,%zu](%f,%f):alpha:%f (negative ghost)\n", 
		   time_ind, geom->mpi_info.rank, (dim==X)?"X":(dim==Y? "Y":"Z"), 
		   dim2%2, oi, ii, u1[u_inner_sheet_offset_o+u_inner_offset*ii], 
		   u, alpha[alpha_offset+u_inner_offset*ii]);
	  }
          
	  if (ghosted)
	  {
	    printf("(%zu)%d|%s[%d][%zu,%zu](%f,%f):alpha:%f -> %f (ghosted)\n", 
		   time_ind, geom->mpi_info.rank, (dim==X)?"X":(dim==Y? "Y":"Z"), 
		   dim2%2, oi, ii, \
                   species->ghost_values_receive[dim2][dsi][buffer_offset+ii], 
		   u, ghost_alpha[species->n[idim]*oi+ii], u1[u_offset]);
	  }
#endif	    
	}
      }
      
      // If dimension is Y or Z we need to fill in for ghost values for all 
      // outer limits inner loops values
      if ((dim==Y || dim==Z) && ghosted)
      {
        unsigned int ois[2] = {0, species->n[odim]-1};
        unsigned int i;
        for (i=0; i<2; i++)
        {
          
          // Get one of the two outer indices
          oi = ois[i];
          buffer_offset = species->n[idim]*oi;
          u_offset_o = u_offset_0+u_outer_offset* oi;

          // Iterate over inner indices
          for (ii=0; ii<species->n[idim]; ii++)
          {
            u_offset = u_offset_o+u_inner_offset*ii;
            du[u_offset] += ghost_alpha[species->n[idim]*oi+ii]*        \
              (species->ghost_values_receive[dim2][dsi][buffer_offset+ii]-u1[u_offset]);
          }
        }
      }

      // If dimensions is Z we also need to fill in for 
      if (dim==Z && ghosted)
      {
        unsigned int iis[2] = {0, species->n[idim]-1};
        unsigned int i;
        
        // Iterate over outer indices
        for (oi=1; oi<species->n[odim]-1; oi++)
        {

          // Get offset dependent on the outer index
          buffer_offset = species->n[idim]*oi;
          u_offset_o = u_offset_0+u_outer_offset* oi;

          // Iterate over the two inner indices
          for (i=0; i<2; i++)
          {
          
            // Get one of the two inner indices
            ii = iis[i];
            u_offset = u_offset_o+u_inner_offset*ii;
            du[u_offset] += ghost_alpha[species->n[idim]*oi+ii]*        \
              (species->ghost_values_receive[dim2][dsi][buffer_offset+ii]-u1[u_offset]);
          }        
        }          
      }
    }
  }
  pop_time(BORDERS);
}
//-----------------------------------------------------------------------------
void Species_step_reaction(Species_t* species, size_t time_ind)
{
  Geometry_t* geom = species->geom;
  domain_id bsi0, bsi1;
  unsigned int bi, di, xi, yi, zi;
  unsigned int i_offset[NDIMS];
  unsigned int xi_offset_geom, yi_offset_geom, zi_geom;
  REAL J, b0, b1;
  REAL dt = species->dt;
  
  // If substeping
  if (species->num_all_buffers==0 || time_ind % species->reaction_substep != 0)
    return;

  // Flag all species to be updated
  // FIXME: Moved to inside Reaction loop.
  //for (bi=0; bi<species->num_species; bi++)
  //  species->update_species[bi] = 1;

  push_time(REACT);

  // Scale time step with number of substeps
  dt *= species->reaction_substep;

  // Needed for vectorization
  size_t nz = species->n[Z];

  // Iterate over the interior X indices
  for (xi=0; xi<species->n[X]; xi++)
  {

    // Compute the X offsets 
    i_offset[X] = xi*species->n[Y]*species->n[Z];

    // Geom offset
    xi_offset_geom = (xi/geom->subdivisions)*geom->n[Z]*geom->n[Y];
    
    // Iterate over the interior Y indices
    for (yi=0; yi<species->n[Y]; yi++)
    {

      // Compute the Y offsets 
      i_offset[Y] =  yi*species->n[Z];

      // Geom offset
      yi_offset_geom = (yi/geom->subdivisions)*geom->n[Z];

      // Iterate over all buffer species
      for (bi=0; bi<species->num_all_buffers; bi++)
//	for (zi=0; zi<nz; zi++)
      {
	
	// Try to get this vectorized by iterating over let say two domain 
        // voxels at a time if we know they are adjacent we vectorize if not we do not
//#pragma simd 
//#pragma prefetch
        //REAL J_max=0;
        //char* b_name = NULL;
        //REAL bmax_0=0.0, bmax_1=0.0;
	for (zi=0; zi<nz; zi++)
//      for (bi=0; bi<species->num_all_buffers; bi++)
	{
	  zi_geom = zi/geom->subdivisions;
	  di = geom->domains[xi_offset_geom+yi_offset_geom+zi_geom];
	  bsi0 = species->bsp0[di][bi];
	  bsi1 = species->bsp1[di][bi];
	  b0 = species->u1[bsi0][i_offset[X]+i_offset[Y]+zi];
	  b1 = species->u1[bsi1][i_offset[X]+i_offset[Y]+zi];
	  J = species->k_on[di][bi]*(species->tot[di][bi]-b0)*b1 - species->k_off[di][bi]*b0;

          // Flag species to be updated
          species->update_species[bsi0] = 1;
          species->update_species[bsi1] = 1;

          // Update species
          species->du[bsi0][i_offset[X]+i_offset[Y]+zi] += dt*J;
          species->du[bsi1][i_offset[X]+i_offset[Y]+zi] -= dt*J;
	  //species->u1[bsi0][i_offset[X]+i_offset[Y]+zi] += dt*J;
	  //species->u1[bsi1][i_offset[X]+i_offset[Y]+zi] -= dt*J;

          //if (fabs(J*dt)>J_max*dt)
          //{
          //  J_max =  fabs(J)*dt;
          //  b_name = species->species_names[bsi0];
          //  bmax_0 = b0;
          //  bmax_1 = b1;
          //}
	}
        //printf("Max J: %s->%e|%e|%e\n", b_name, J_max, bmax_0, bmax_1);
      }
    }
  }
  pop_time(REACT);
}
//-----------------------------------------------------------------------------
void Species_step_boundary_fluxes(Species_t* species, size_t time_ind)
{
  
  // FIXME: This function only update fluxes for the first species!!
  // FIXME: Need a more general way to handle this...
  unsigned int* loc_boundary_voxels;
  unsigned int yz_offset, y_offset, xi, yi, zi, u0_offset, u1_offset;
  Geometry_t* geom = species->geom;
  unsigned int dbi=0, bi, bvi, si=0;
  REAL J;
  
  // FIXME: Make substepping work
  // If substeping
  //if (time_ind % species->species_substeps[dsi] != 0)
  //  continue;

  // Species values on both side of the boundaries
  REAL u0;
  REAL u1;

  // A function pointer for the flux evaluation
  REAL (*flux_func)(REAL, REAL, REAL , REAL , REAL* );

  // Pointer to the flux parameters
  REAL* params;

  // Get offsets 
  yz_offset = species->n[Y]*species->n[Z];
  y_offset  = species->n[Z];

  push_time(BOUNDARIES);

  // Iterate over all boundaries
  for (bi=0; bi<geom->num_boundaries; bi++)
  {

    // Get flux function
    // FIXME: Only accept serca and ryr and stuff is hardcoded...
    if (!strcmp(geom->boundary_names[bi], "ryr"))
    {
      flux_func = &ryr_flux;
      params = species->boundary_fluxes->ryr.params;
    }

    else if (!strcmp(geom->boundary_names[bi], "serca"))
    {
      flux_func = &serca_flux;
      params = species->boundary_fluxes->serca.params;
    }

    else
      continue;

    // Iterate over all boundary voxels
    for (bvi=0; bvi<species->num_boundary_voxels[bi]; bvi++)
    {
      
      // If discrete and not open continue
      if (geom->boundary_types[bi]==discrete &&                 \
          !geom->open_local_discrete_boundaries[dbi][bvi])
      {
        continue;
      }

      // Get start of indices
      loc_boundary_voxels = &species->boundary_voxels[bi][bvi*2*NDIMS];
      
      // Collect from voxel indices
      xi = loc_boundary_voxels[X];
      yi = loc_boundary_voxels[Y];
      zi = loc_boundary_voxels[Z];
#if DEBUG
      printf("rank[%d] %s FROM: %d:[%d,%d,%d]=%.2f\n", geom->mpi_info.rank, 
             geom->boundary_names[bi], bvi, xi, yi, zi,                   \
             species->u1[si][xi*yz_offset+yi*y_offset+zi]);
#endif
      u0_offset = xi*yz_offset+yi*y_offset+zi;
      u0 = species->u1[si][u0_offset];

      // Increase pointer
      loc_boundary_voxels += NDIMS;
      
      // Collect to voxel indices
      xi = loc_boundary_voxels[X];
      yi = loc_boundary_voxels[Y];
      zi = loc_boundary_voxels[Z];
#if DEBUG
      printf("rank[%d] %s   To: %d:[%d,%d,%d]=%.2f\n", geom->mpi_info.rank, 
             geom->boundary_names[bi], bvi, xi, yi, zi,                   \
             species->u1[si][xi*yz_offset+yi*y_offset+zi]);
#endif

      u1_offset = xi*yz_offset+yi*y_offset+zi;
      u1 = species->u1[si][u1_offset];

      // Call flux function
      J = flux_func(species->dt, geom->h, u0, u1, params);

      // Apply flux from u0 to u1
      species->du[si][u0_offset] -= J;
      species->du[si][u1_offset] += J;

    }

    // Increase dbi counter if boundary is discrete
    dbi += geom->boundary_types[bi]==discrete ? 1 : 0;

  }

  pop_time(BOUNDARIES);
}
//-----------------------------------------------------------------------------
void Species_evaluate_stochastic_events(Species_t* species, size_t time_ind)
{

  Geometry_t* geom = species->geom;
  unsigned int dbi, bi;

  // If substeping 
  if (geom->num_global_discrete_boundaries==0 || \
      time_ind % species->stochastic_substep != 0)
    return;

  // Communicate species values at discrete boundaries to processor 0
  push_time(STOCH);
  Species_communicate_values_at_discrete_boundaries(species);

  // Iterate over discrete boundaries
  for (dbi=0; dbi<geom->num_global_discrete_boundaries; dbi++)
  {
      
    // Get the boundary index
    bi = geom->discrete_boundary_indices[dbi];

    // We hardcode ryr update
    if (!strcmp(geom->boundary_names[bi], "ryr"))
    {
        
      // Iterate over discrete bondaries on processor 0 and evaluate stochastic events
      if (geom->mpi_info.rank == 0)
      {
    
        // Get local pointer to ryr
        RyR_model_t* ryr = &species->boundary_fluxes->ryr;

        // Evaluate hard coded RyR stochastic update
        evaluate_RyR_stochastically(ryr, time_ind*species->dt, 
           species->dt*species->stochastic_substep, 
           geom->species_values_at_local_discrete_boundaries_correct_order_rank_0[dbi]);
        
      }
    }
  }

  // Communicate the openness of the discrete boundaries from rank 0 to other processes
  Species_communicate_openness_of_discrete_boundaries(species);

  pop_time(STOCH);
}
//-----------------------------------------------------------------------------
void Species_update_ghost(Species_t* species, size_t time_ind)
{
  
  // FIXME: Update ghost only for species with correct substep

  // If no ghost values
  if (!(species->geom->mpi_info.size>1))
    return;

  push_time(GHOST);
  Geometry_t* geom = species->geom;

  size_t dim, dim2, dsi, oi, ii, si;
  size_t odim, idim;
  REAL* u;

  size_t buffer_offset;
  size_t u_offset_i, u_offset, u_outer_offset, u_inner_offset;

  // Allocate ghost values
  // Update each diffusive species
  for (dsi=0; dsi<species->num_all_diffusive; dsi++)
  {

    // If substeping 
    if (time_ind % species->species_substeps[dsi] != 0)
      continue;

    // Get corresponding species ind
    si = species->all_diffusive[dsi];
	
    // Get local species
    u = species->u1[si];
	
    for (dim2=0; dim2<NDIMS*2; dim2++)
    {
    
      // Get X, Y, Z dim
      dim = dim2/2;
    
      // Get the dimension of the outer and inner loops
      odim = dim != X ? X : Y;
      idim = dim != Z ? Z : Y;

      // If we have a neighbor
      if (geom->mpi_info.neighbors[dim2]!=MPI_PROC_NULL)
      {
      
        // Get u offsets
        u_offset = species->u_offsets[dim2];
        u_inner_offset = species->u_inner_offsets[dim2];
        u_outer_offset = species->u_outer_offsets[dim2];

	// Copy value to send buffer
	for (oi=0; oi<species->n[odim]; oi++)
	{
	  
	  // Offset 
	  buffer_offset = species->n[idim]*oi;
	  u_offset_i = u_offset+u_outer_offset*oi;
	  for (ii=0; ii<species->n[idim]; ii++)
	  {
	    species->ghost_values_send[dim2][dsi][buffer_offset+ii] = \
	      u[u_offset_i+u_inner_offset*ii];
	    //printf("%d|%s[%zu] %f\n", geom->mpi_info.rank, (dim==X)?"X":(dim==Y? "Y":"Z"), dim2%2, u[u_offset_i+u_inner_offset*ii]);
	  }
	}

      }

      // Communicate
      MPI_Isend(species->ghost_values_send[dim2][dsi],
                species->size_ghost_values[dim2], 
                MPIREAL, geom->mpi_info.neighbors[dim2], 
                1000*dim*dsi,
                geom->mpi_info.comm, &geom->mpi_info.send_req[dim2]);
      
      MPI_Irecv(species->ghost_values_receive[dim2][dsi],
                species->size_ghost_values[dim2], 
                MPIREAL, geom->mpi_info.neighbors[dim2], 
                1000*dim*dsi,
                geom->mpi_info.comm, &geom->mpi_info.receive_req[dim2]);
    }
    // Wait for all send and receive calls (per diffusive species) to end communication
    MPI_Waitall(NDIMS*2, geom->mpi_info.send_req, MPI_STATUS_IGNORE);
    MPI_Waitall(NDIMS*2, geom->mpi_info.receive_req, MPI_STATUS_IGNORE);

  }
 
  /*
  for (dim2=0; dim2<NDIMS*2; dim2++)
  {
    
    // Get X, Y, Z dim
    dim = dim2/2;
    
    // Get the dimension of the outer and inner loops
    odim = dim != X ? X : Y;
    idim = dim != Z ? Z : Y;

    if (species->size_ghost_values[dim2]*species->num_all_diffusive>0)
    {
      printf("\n%d|%s[%zu]:", geom->mpi_info.rank, (dim==X)?"X":(dim==Y? "Y":"Z"), dim2%2);
      for (ii=0; ii<species->size_ghost_values[dim2]*species->num_all_diffusive;ii++)
	printf("%.2f,",species->ghost_values_receive[dim2][ii]);
      printf("\n\n");
    }
  }
  */
  pop_time(GHOST);
}
//-----------------------------------------------------------------------------
void Species_output_init_values(Species_t* species)
{
  unsigned int i, j, k, bi, di, si;
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;

  mpi_printf0(comm, "\n");
  mpi_printf0(comm, "Model parameters:\n");
  mpi_printf0(comm, "-----------------------------------------------------------------------------\n");

  mpi_printf0(comm, "%8s", "Init:");
  for (i=0; i<species->num_species; i++)
    mpi_printf0(comm, "%13s", species->species_names[i]);
  mpi_printf0(comm, "\n");
  for (i=0; i<geom->num_domains; i++)
  {
    mpi_printf0(comm, "%7s:", geom->domain_names[i]);
    for (j=0; j<species->num_species; j++)
    {
      if (species->init[i][j]>0)
	mpi_printf0(comm, "%13.2e", species->init[i][j]);
      else
	mpi_printf0(comm, "%13s", "-");
    }
    mpi_printf0(comm, "\n");
  }

  mpi_printf0(comm, "\n");
  mpi_printf0(comm, "%8s", "Sigma:");
  for (i=0; i<species->num_all_diffusive; i++)
    mpi_printf0(comm, "%13s", species->species_names[species->all_diffusive[i]]);
  mpi_printf0(comm, "\n");
  for (i=0; i<geom->num_domains; i++)
  {
    mpi_printf0(comm, "%7s:", geom->domain_names[i]);
    for (j=0; j<species->num_all_diffusive; j++)
    {

      unsigned int jj = species->all_diffusive[j];
      unsigned char diffusive = 0;
      for (k=0; k<species->num_diffusive[i]; k++)
      {
	if (species->diffusive[i][k]==jj)
	{
	  mpi_printf0(comm, "%13.2e", species->sigma[i][k]);
	  diffusive = 1;
	}
      }
      if (!diffusive)
	mpi_printf0(comm, "%13s", "-");
    }
    mpi_printf0(comm, "\n");
  }

  // Print buffer information if any
  if (species->num_all_buffers>0)
  {
    mpi_printf0(comm, "\n");
    mpi_printf0(comm, "%8s", "B_tot:");
    for (i=0; i<species->num_species; i++)
      mpi_printf0(comm, "%13s", species->all_buffers_b[i] ? species->species_names[i] : "");
    mpi_printf0(comm, "\n");
    for (i=0; i<geom->num_domains; i++)
    {
      mpi_printf0(comm, "%7s:", geom->domain_names[i]);
      bi = 0;
      for (j=0; j<species->num_species; j++)
      {
    
        if (species->all_buffers_b[j])
        {
    
    	if (bi<species->num_buffers[i] && species->bsp0[i][bi]==j)
    	  mpi_printf0(comm, "%13.2e", species->tot[i][bi++]);
    	else
    	  mpi_printf0(comm, "%13s", "-");
        }
        else
	  mpi_printf0(comm, "%13s", "");
      }
      mpi_printf0(comm, "\n");
    }
    
    mpi_printf0(comm, "\n");
    mpi_printf0(comm, "%8s", "k_off:");
    for (i=0; i<species->num_species; i++)
      mpi_printf0(comm, "%13s", species->all_buffers_b[i] ? species->species_names[i] : "");
    mpi_printf0(comm, "\n");
    for (i=0; i<geom->num_domains; i++)
    {
      mpi_printf0(comm, "%7s:", geom->domain_names[i]);
      bi = 0;
      for (j=0; j<species->num_species; j++)
      {
    
        if (species->all_buffers_b[j])
        {
    
    	if (bi<species->num_buffers[i] && species->bsp0[i][bi]==j)
    	  mpi_printf0(comm, "%13.2e", species->k_off[i][bi++]);
    	else
    	  mpi_printf0(comm, "%13s", "-");
        }
        else
	  mpi_printf0(comm, "%13s", "");
      }
      mpi_printf0(comm, "\n");
    }
    
    mpi_printf0(comm, "\n");
    mpi_printf0(comm, "%8s", "k_on:");
    for (i=0; i<species->num_species; i++)
      mpi_printf0(comm, "%13s", species->all_buffers_b[i] ? species->species_names[i] : "");
    mpi_printf0(comm, "\n");
    for (i=0; i<geom->num_domains; i++)
    {
      mpi_printf0(comm, "%7s:", geom->domain_names[i]);
      bi = 0;
      for (j=0; j<species->num_species; j++)
      {
    
        if (species->all_buffers_b[j])
        {
    
    	if (bi<species->num_buffers[i] && species->bsp0[i][bi]==j)
    	  mpi_printf0(comm, "%13.2e", species->k_on[i][bi++]);
    	else
    	  mpi_printf0(comm, "%13s", "-");
        }
        else
	  mpi_printf0(comm, "%13s", "");
      }
      mpi_printf0(comm, "\n");
    }
  }

  if (species->num_fixed_domains>0)
  {
    
    mpi_printf0(comm, "\n");
    mpi_printf0(comm, "%8s\n", "Fixed domain species:");
    for (i=0; i<species->num_fixed_domains; i++)
    {
      di = species->fixed_domains[2*i];
      si = species->fixed_domains[2*i+1];
      mpi_printf0(comm, "%8s:%8s\n", geom->domain_names[di], species->species_names[si]);
    }
  }
}
//-----------------------------------------------------------------------------
void Species_output_data(Species_t* species, unsigned int output_ind, REAL t, 
			 unsigned long time_ind, arguments_t* arguments)
{
  
  char filename[MAX_FILE_NAME];
  char groupname[MAX_SPECIES_NAME];
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;
  hid_t file_id=0, plist_id=0, group_id=0;
  
  sprintf(filename, "%s.h5", arguments->casename);
  sprintf(groupname, "/data_%07d", output_ind);

  if (geom->mpi_info.rank==0)
  {

    plist_id = H5Pcreate(H5P_FILE_ACCESS);

    // If first time create file
    if (output_ind==0)
      file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);

    // If not first time open file
    else
      file_id = H5Fopen(filename, H5F_ACC_RDWR, plist_id);

    // Create data group
    group_id = H5Gcreate(file_id, groupname, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    // Write time and time index
    write_h5_attr(comm, group_id, "time", H5REAL, 1, &t);
    write_h5_attr(comm, group_id, "time_index", H5T_STD_U64LE, 1, &time_ind);
    
  }

  // Write scalar data to file
  Species_output_scalar_data(species, file_id, groupname, t, time_ind, output_ind, 
                             arguments->silent);
  
  if (geom->mpi_info.rank==0)
  {
    H5Gclose(group_id);
    H5Pclose(plist_id);
    H5Fclose(file_id);
  }

  // Check for saving 2D sheets
  if (arguments->num_save_species)
  {

    // Open the save file in collective mode
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, comm, MPI_INFO_NULL);
    file_id = H5Fopen(filename, H5F_ACC_RDWR, plist_id);

    // Get center index
    Species_write_2D_sheet_to_file(species, file_id, groupname);
    
#if DEBUG
    if (output_ind==0)
      Species_write_alpha_values_to_file(species, file_id);
#endif

    H5Pclose(plist_id);
    H5Fclose(file_id);
  }
}
//-----------------------------------------------------------------------------
void Species_write_2D_sheet_to_file(Species_t* species, hid_t file_id, char* groupname)
{
  
  Geometry_t* geom = species->geom;

  size_t ssi, si, xi, yi, zi, xi_offset_data, xi_offset;
  char datasetname[MAX_FILE_NAME];

  // Full data set dimension
  hsize_t dimsf[2] = {species->N[X], species->N[Y]};

  // Local chunk dimension, count and offset into dataset
  hsize_t chunk_dims_full[2] = {species->n[X], species->n[Y]};
  hsize_t chunk_dims_empty[2] = {0, 0};
  hsize_t* chunk_dims;
  hsize_t count[2] = {1, 1};
  hsize_t offset[2] = {geom->offsets[X]*geom->subdivisions, 
		       geom->offsets[Y]*geom->subdivisions};
  hsize_t stride[2] = {1,1};

  //mpi_printf0(comm, "dimsf: [%zu,%zu]\n", dimsf[0], dimsf[1]);
  //printf("%d: chunk dims: [%zu,%zu]\n", geom->mpi_info.rank, chunk_dims[0], chunk_dims[1]);
  //printf("%d: offset: [%zu,%zu]\n", geom->mpi_info.rank, offset[0], offset[1]);

  // Iterate over the save species and save then using local chunks
  for (ssi=0; ssi<species->num_save_species; ssi++)
  {
  
    // Get species
    si = species->ind_save_species[ssi];

    for (zi=0; zi<species->num_sheets_per_species; zi++)
    {

      if (species->z_indices[zi]>=0)
      {
        
        chunk_dims = chunk_dims_full;

        // Copy data to transfer memory
        for (xi=0; xi<species->n[X]; xi++)
        {
          xi_offset_data = xi*species->n[Y]*species->n[Z];
          xi_offset = xi*species->n[Y];
          for (yi=0; yi<species->n[Y]; yi++)
          {
            species->u1_sheet_save[xi_offset+yi] = \
              species->u1[si][xi_offset_data+yi*species->n[Z]+species->z_indices[zi]];
            //mpi_printf0(geom->mpi_info.comm, "%f\n", species->u1_sheet_save[xi_offset+yi]);
          }
        }
      }
      else
      {
        chunk_dims = chunk_dims_empty;
      }

      // Create data field name
      sprintf(datasetname, "%s/%s_sheet_%d", groupname, species->species_names[si],
              species->z_global_indices[zi]);

      // file and dataset identifiers
      hid_t filespace = H5Screate_simple(2, dimsf, NULL); 
      //printf("created filespace:\n");
      hid_t memspace  = H5Screate_simple(2, chunk_dims, NULL); 
      //printf("created memspace:\n");
      
      // Create chunked dataset. 
      hid_t plist_id = H5Pcreate(H5P_DATASET_CREATE);
      H5Pset_chunk(plist_id, 2, chunk_dims);
      //printf("created chunked dataset0:\n");
      
      // Create compression filter (Not supported in parallel yet...)
      //unsigned int gzip_level = 9;
      //herr_t status = H5Pset_filter(plist_id, H5Z_FILTER_DEFLATE, 
      //				  H5Z_FLAG_OPTIONAL, 1, &gzip_level);
      
      hid_t dset_id = H5Dcreate(file_id, datasetname, H5REAL, filespace, 
          		      H5P_DEFAULT, plist_id, H5P_DEFAULT);
      
      //printf("created chunked dataset1:\n");
      H5Pclose(plist_id);
      H5Sclose(filespace);
      
      // Select hyperslab in the file.
      filespace = H5Dget_space(dset_id);
      //printf("get data space\n");
      H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, stride, count, \
          		chunk_dims);
      
      //printf("create hyper slab\n");
      // Create property list for collective dataset write.
      plist_id = H5Pcreate(H5P_DATASET_XFER);
      H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
      
      //printf("save data\n");
      H5Dwrite(dset_id, H5REAL, memspace, filespace, plist_id, species->u1_sheet_save);
      
      //printf("data saved\n");
      
      // Close/release resources.
      H5Sclose(filespace);
      H5Pclose(plist_id);
      H5Dclose(dset_id);
      H5Sclose(memspace);

    }
  }
}
//-----------------------------------------------------------------------------
void Species_write_alpha_values_to_file(Species_t* species, hid_t file_id)
{
  
  Geometry_t* geom = species->geom;
  //MPI_Comm comm = geom->mpi_info.comm;

  // Create data group
  hid_t group_id = H5Gcreate(file_id, "/alphas", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  size_t dim, dsi, si;
  char datasetname[MAX_FILE_NAME];

  // Full data set dimension
  hsize_t dimsf[3] = {species->N[X], species->N[Y], species->N[Z]};

  // Local chunk dimension, count and offset into dataset
  hsize_t chunk_dims[3] = {species->n[X], species->n[Y], species->n[Z]};
  hsize_t count[3] = {1, 1, 1};
  hsize_t offset[3] = {geom->offsets[X]*geom->subdivisions, 
		       geom->offsets[Y]*geom->subdivisions,
		       geom->offsets[Z]*geom->subdivisions};
  hsize_t stride[3] = {1,1,1};

  //mpi_printf0(comm, "dimsf: [%zu,%zu]\n", dimsf[0], dimsf[1]);
  //printf("%d: chunk dims: [%zu,%zu]\n", geom->mpi_info.rank, chunk_dims[0], chunk_dims[1]);
  //printf("%d: offset: [%zu,%zu]\n", geom->mpi_info.rank, offset[0], offset[1]);

  // Iterate over the save species and save then using local chunks
  for (dsi=0; dsi<species->num_all_diffusive; dsi++)
  {
  
    // Get species
    si = species->all_diffusive[dsi];

    for (dim=0; dim<NDIMS; dim++)
    {
    
      // Create data field name
      sprintf(datasetname, "/alphas/%s_%s", species->species_names[si], \
	      (dim==X)?"X":(dim==Y? "Y":"Z"));
      
      //printf("outputting: %s\n", datasetname);
      
      // file and dataset identifiers
      hid_t filespace = H5Screate_simple(3, dimsf, NULL); 
      //printf("created filespace:\n");
      hid_t memspace  = H5Screate_simple(3, chunk_dims, NULL); 
      //printf("created memspace:\n");
      
      // Create chunked dataset. 
      hid_t plist_id = H5Pcreate(H5P_DATASET_CREATE);
      H5Pset_chunk(plist_id, 3, chunk_dims);
      //printf("created chunked dataset0:\n");
      
      // Create compression filter (Not supported in parallel yet...)
      //unsigned int gzip_level = 9;
      //herr_t status = H5Pset_filter(plist_id, H5Z_FILTER_DEFLATE, 
      //				  H5Z_FLAG_OPTIONAL, 1, &gzip_level);
      
      hid_t dset_id = H5Dcreate(file_id, datasetname, H5REAL, filespace, 
      			      H5P_DEFAULT, plist_id, H5P_DEFAULT);
      
      //printf("created chunked dataset1:\n");
      H5Pclose(plist_id);
      H5Sclose(filespace);
      
      // Select hyperslab in the file.
      filespace = H5Dget_space(dset_id);
      //printf("get data space\n");
      H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, stride, count, \
			  chunk_dims);
      
      //printf("create hyper slab\n");
      // Create property list for collective dataset write.
      plist_id = H5Pcreate(H5P_DATASET_XFER);
      H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
      
      //printf("save data\n");
      H5Dwrite(dset_id, H5REAL, memspace, filespace, plist_id, species->alpha[dim][dsi]);
      
      //printf("data saved\n");
      
      // Close/release resources.
      H5Sclose(filespace);
      H5Pclose(plist_id);
      H5Dclose(dset_id);
      H5Sclose(memspace);

    }

   // Create data field name
   sprintf(datasetname, "/alphas/u0_%s", species->species_names[si]);
   
   //printf("outputting: %s\n", datasetname);
   
   // file and dataset identifiers
   hid_t filespace = H5Screate_simple(3, dimsf, NULL); 
   //printf("created filespace:\n");
   hid_t memspace  = H5Screate_simple(3, chunk_dims, NULL); 
   //printf("created memspace:\n");
   
   // Create chunked dataset. 
   hid_t plist_id = H5Pcreate(H5P_DATASET_CREATE);
   H5Pset_chunk(plist_id, 3, chunk_dims);
   //printf("created chunked dataset0:\n");
   
   // Create compression filter (Not supported in parallel yet...)
   //unsigned int gzip_level = 9;
   //herr_t status = H5Pset_filter(plist_id, H5Z_FILTER_DEFLATE, 
   //				  H5Z_FLAG_OPTIONAL, 1, &gzip_level);
   
   hid_t dset_id = H5Dcreate(file_id, datasetname, H5REAL, filespace, 
   			      H5P_DEFAULT, plist_id, H5P_DEFAULT);
   
   //printf("created chunked dataset1:\n");
   H5Pclose(plist_id);
   H5Sclose(filespace);
   
   // Select hyperslab in the file.
   filespace = H5Dget_space(dset_id);
   //printf("get data space\n");
   H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, stride, count, \
   			  chunk_dims);
   
   //printf("create hyper slab\n");
   // Create property list for collective dataset write.
   plist_id = H5Pcreate(H5P_DATASET_XFER);
   H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
   
   //printf("save data\n");
   H5Dwrite(dset_id, H5REAL, memspace, filespace, plist_id, species->u1[si]);
   
   //printf("data saved\n");
   
   // Close/release resources.
   H5Sclose(filespace);
   H5Pclose(plist_id);
   H5Dclose(dset_id);
   H5Sclose(memspace);

  }

  H5Gclose(group_id);
}
//-----------------------------------------------------------------------------
void Species_communicate_values_at_discrete_boundaries(Species_t* species)
{
  
  int dbi, dbj, bi, sendcount;
  unsigned int yz_offset, y_offset, xi, yi, zi;
  unsigned int* loc_boundary_voxels;
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;
  REAL* send_buff;
  REAL* receive_buff = NULL;
  int* displs = NULL;
  int* recvcounts = NULL;

  // Only consider "Ca or the first species"
  REAL* u = species->u1[0];
  
  // Iterate over discrete boundaries
  yz_offset = species->n[Y]*species->n[Z];
  y_offset  = species->n[Z];

  for (dbi=0; dbi<geom->num_global_discrete_boundaries; dbi++)
  {
    
    // Get the boundary index
    bi = geom->discrete_boundary_indices[dbi];

    // Iterate over the local boundaries and collect species values
    for (dbj=0; dbj<geom->local_boundary_size[bi]; dbj++)
    {

      // Get start of indices
      loc_boundary_voxels = &species->boundary_voxels[bi][dbj*2*NDIMS];
      
      // Collect from voxel indices
      xi = loc_boundary_voxels[X];
      yi = loc_boundary_voxels[Y];
      zi = loc_boundary_voxels[Z];
#if DEBUG
      printf("FROM: %d|%d:[%d,%d,%d]=%.2f\n", dbi, dbj, xi, yi, zi, \
             u[xi*yz_offset+yi*y_offset+zi]);
#endif
      // FIXME: Consider moving this to Species_t
      geom->species_values_at_local_discrete_boundaries[dbi][dbj*2] = \
        u[xi*yz_offset+yi*y_offset+zi];

      // Increase pointer
      loc_boundary_voxels += NDIMS;
      
      // Collect to voxel indices
      xi = loc_boundary_voxels[X];
      yi = loc_boundary_voxels[Y];
      zi = loc_boundary_voxels[Z];
#if DEBUG
      printf("TO:   %d|%d:[%d,%d,%d]=%.2f\n", dbi, dbj, xi, yi, zi, \
             u[xi*yz_offset+yi*y_offset+zi]);
#endif

      geom->species_values_at_local_discrete_boundaries[dbi][dbj*2+1] = \
        u[xi*yz_offset+yi*y_offset+zi];
      
    }
    
    // Assign Gatherv arguments
    send_buff = geom->species_values_at_local_discrete_boundaries[dbi]; 
    sendcount = geom->num_local_discrete_boundaries[dbi]*2;
    recvcounts = geom->num_local_species_values_at_discrete_boundaries_rank_0[dbi];
    displs = geom->offset_num_species_values_discrete_boundaries_rank_0[dbi];
    receive_buff = geom->species_values_at_local_discrete_boundaries_rank_0[dbi];
    
    // Do the assymetric MPI call
    MPI_Gatherv(send_buff, sendcount, MPIREAL,
                receive_buff, recvcounts, displs, MPIREAL, 0, comm);

    // Transfer species values to globally correct order
    if (geom->mpi_info.rank==0)
    {
      
      int gdbi;
#if DEBUG
      printf("Receiving discrete values: ");
#endif
      for (dbj=0; dbj<geom->num_discrete_boundaries[dbi]; dbj++)
      {

        // Get global index number for discrete boundary
        gdbi = geom->local_distribution_of_global_discrete_boundaries_rank_0[dbi][dbj];

        // Transfer the two values 
        // FIXME: Make this more flexible in the future by transfering maybe all 
        // FIXME: species values?
        geom->species_values_at_local_discrete_boundaries_correct_order_rank_0\
          [dbi][gdbi*2] = geom->species_values_at_local_discrete_boundaries_rank_0[dbi][dbj*2];
        geom->species_values_at_local_discrete_boundaries_correct_order_rank_0\
          [dbi][gdbi*2+1] = geom->species_values_at_local_discrete_boundaries_rank_0[dbi][dbj*2+1];
#if DEBUG
        printf("%.2f, ", geom->species_values_at_local_discrete_boundaries_rank_0[dbi][dbj*2]);
        printf("%.2f, ", geom->species_values_at_local_discrete_boundaries_rank_0[dbi][dbj*2+1]);
#endif

      }      
#if DEBUG
      printf("\n\n");
#endif
    }
  }
}
//-----------------------------------------------------------------------------
void Species_communicate_openness_of_discrete_boundaries(Species_t* species)
{
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;
  unsigned int bi, dbi, i, gdbi;
  int* send_buff;
  int* send_counts;
  int* displs;
  int* receive_buff;
  int recv_count;

  // Iterate over discrete boundaries
  for (dbi=0; dbi<geom->num_global_discrete_boundaries; dbi++)
  {
      
    // We hardcode ryr update
    // FIXME: Do not...
    bi = geom->discrete_boundary_indices[dbi];

    if (!strcmp(geom->boundary_names[bi], "ryr"))
    {
        
      // Get local pointer to ryr
      RyR_model_t* ryr = &species->boundary_fluxes->ryr;

      // Iterate over discrete bondaries on processor 0 and gather openness
      if (geom->mpi_info.rank == 0)
      {
    
        // Iterate each discrete boundary
        for (i=0; i<geom->num_discrete_boundaries[dbi]; i++)
        {

          // Get global index number for discrete boundary
          gdbi = geom->local_distribution_of_global_discrete_boundaries_rank_0[dbi][i];

          // Evaluate the openess of that ryr
          geom->open_local_discrete_boundaries_rank_0[dbi][i] = open_RyR(ryr, gdbi);
          
        }
      }

      // Communicate the opennes of the discrete boundaries to the local processes
      send_buff = geom->open_local_discrete_boundaries_rank_0[dbi];
      send_counts = geom->num_local_discrete_boundaries_rank_0[dbi];
      displs = geom->offset_num_local_discrete_boundaries_rank_0[dbi];
      receive_buff = geom->open_local_discrete_boundaries[dbi];
      recv_count = geom->num_local_discrete_boundaries[dbi];

      // Communicate the opennes from process 0 to all other processes
      MPI_Scatterv(send_buff, send_counts, displs, MPI_INT, 
                   receive_buff, recv_count, MPI_INT,
                   0, comm);
    }
  }
}
//-----------------------------------------------------------------------------
void Species_output_scalar_data(Species_t* species, hid_t file_id, char* groupname, 
				REAL t, unsigned long time_ind, unsigned long output_ind, 
                                domain_id silent)
{
  
  char domain_groupname[MAX_FILE_NAME];
  char species_groupname[MAX_FILE_NAME];
  char datasetname[MAX_FILE_NAME];
  unsigned int i, bi, dbi, xi, yi, zi, xi_offset, yi_offset, xi_offset_geom;
  unsigned int yi_offset_geom, zi_geom, di_si;

  hid_t domain_group_id, species_group_id;
  domain_id di, si;
  Geometry_t* geom = species->geom;
  int rank = geom->mpi_info.rank;
  MPI_Comm comm = geom->mpi_info.comm;
  MPIInfo* mpi_info = &geom->mpi_info;
  REAL* buff = NULL;
  REAL value;
  unsigned int num_domain_species = geom->num_domains*species->num_species;

  // Local buffers
  REAL* max_values = mpi_malloc(comm, sizeof(REAL)*num_domain_species);
  memfill(max_values, num_domain_species, FLT_MIN);

  REAL* min_values = mpi_malloc(comm, sizeof(REAL)*num_domain_species);
  memfill(min_values, num_domain_species, FLT_MAX);

  REAL* total_values = mpi_malloc(comm, sizeof(REAL)*num_domain_species);
  memfill(total_values, num_domain_species, 0.);

  // Allocate memory for communication buffer
  if (rank==0)
    buff = mpi_malloc(comm, sizeof(REAL)*num_domain_species*mpi_info->size);

  // Iterate over species and collect min, max and total values
  for (si=0; si<species->num_species; si++)
  {

    for (xi=0; xi<species->n[X]; xi++)
    {

      xi_offset_geom = (xi/geom->subdivisions)*geom->n[Y]*geom->n[Z];
      xi_offset = xi*species->n[Y]*species->n[Z];
      for (yi=0; yi<species->n[Y]; yi++)
      {
	yi_offset_geom = (yi/geom->subdivisions)*geom->n[Z];
	yi_offset = yi*species->n[Z];
	for (zi=0; zi<species->n[Z]; zi++)
	{
	  zi_geom = zi/geom->subdivisions;
	  di = geom->domains[xi_offset_geom+yi_offset_geom+zi_geom];
	  assert(di<geom->num_domains);
	  value = species->u1[si][xi_offset+yi_offset+zi];
	  di_si = di*species->num_species+si;
	  min_values[di_si] = value < min_values[di_si] ? value : min_values[di_si];
	  max_values[di_si] = value > max_values[di_si] ? value : max_values[di_si];
	  total_values[di_si] += value*species->dV;
	}
      }
    }
  }

  // Send all values to processor 0
  MPI_Gather(total_values, num_domain_species, MPIREAL, 
	     buff, num_domain_species, MPIREAL, 0, mpi_info->comm);

  // Collect all values
  if (rank==0)
  {

    // Skip 0 rank
    for (i=1; i<mpi_info->size; i++)
    {
      for (di=0; di<geom->num_domains; di++)
      {
	for (si=0; si<species->num_species; si++)
	{
	  total_values[di*species->num_species+si] +=			\
	    buff[i*num_domain_species+di*species->num_species+si];
	}
      }
    }
  }
  
  // Send all values to processor 0
  MPI_Gather(min_values, num_domain_species, MPIREAL, 
	     buff, num_domain_species, MPIREAL, 0, mpi_info->comm);

  // Collect all values
  if (rank==0)
  {

    // Skip 0 rank
    for (i=1; i<mpi_info->size; i++)
    {
      for (di=0; di<geom->num_domains; di++)
      {
	for (si=0; si<species->num_species; si++)
	{
	  di_si = di*species->num_species+si;
	  value = buff[i*num_domain_species+di*species->num_species+si];
	  min_values[di_si] = value < min_values[di_si] ? value : min_values[di_si];
	}
      }
    }
  }

  // Send all values to processor 0
  MPI_Gather(max_values, num_domain_species, MPIREAL, 
	     buff, num_domain_species, MPIREAL, 0, mpi_info->comm);

  // Collect all values
  if (rank==0)
  {

    // Skip 0 rank
    for (i=1; i<mpi_info->size; i++)
    {
      for (di=0; di<geom->num_domains; di++)
      {
	for (si=0; si<species->num_species; si++)
	{
	  di_si = di*species->num_species+si;
	  value = buff[i*num_domain_species+di*species->num_species+si];
	  max_values[di_si] = value > max_values[di_si] ? value : max_values[di_si];
	}
      }
    }
    
    // Put discrete boundary values on rank 0 in correct order
    for (dbi=0; dbi<geom->num_global_discrete_boundaries; dbi++)
    {

      for (bi=0; bi<geom->num_discrete_boundaries[dbi]; bi++)
      {
        unsigned int gdbi = geom->local_distribution_of_global_discrete_boundaries_rank_0[dbi][bi];
        geom->open_local_discrete_boundaries_correct_order_rank_0[dbi][gdbi] = \
          geom->open_local_discrete_boundaries_rank_0[dbi][bi];
        
      }
    }

    if (silent==0)
    {
      REAL conservation=0.;
      for (di=0; di<geom->num_domains; di++)
        for (si=0; si<species->num_species; si++)
      	conservation += total_values[di*species->num_species+si];
      
      printf("\n%zu: time: %.2f ms, time_index: %zu, conservation: %g\n", \
             output_ind, t, time_ind, conservation);
      printf("-----------------------------------------------------------------------------\n");
      printf("%8s", "Average:");
      for (i=0; i<species->num_species; i++)
        printf("%13s", species->species_names[i]);
      printf("\n");
      for (di=0; di<geom->num_domains; di++)
      {
        printf("%7s:", geom->domain_names[di]);
        for (si=0; si<species->num_species; si++)
        {
      	printf("%13.2e", total_values[di*species->num_species+si]/geom->volumes[di]);
        }
        printf("\n");
      }
      printf("\n");
      
      printf("%8s", "Min:");
      for (i=0; i<species->num_species; i++)
        printf("%13s", species->species_names[i]);
      printf("\n");
      for (di=0; di<geom->num_domains; di++)
      {
        printf("%7s:", geom->domain_names[di]);
        for (si=0; si<species->num_species; si++)
        {
      	printf("%13.2e", min_values[di*species->num_species+si]);
        }
        printf("\n");
      }
      printf("\n");
      
      printf("%8s", "Max:");
      for (i=0; i<species->num_species; i++)
        printf("%13s", species->species_names[i]);
      printf("\n");
      for (di=0; di<geom->num_domains; di++)
      {
        printf("%7s:", geom->domain_names[di]);
        for (si=0; si<species->num_species; si++)
        {
      	printf("%13.2e", max_values[di*species->num_species+si]);
        }
        printf("\n");
      }
      printf("\n");
      
      // Output openess of discrete boundaries
      if (geom->num_global_discrete_boundaries>0)
      {
        printf("%8s", "Channels:\n");
        for (dbi=0; dbi<geom->num_global_discrete_boundaries; dbi++)
        {
        
          printf("%7s: ", geom->boundary_names[geom->discrete_boundary_indices[dbi]]);
          int sum = 0;
          for (bi=0; bi<geom->num_discrete_boundaries[dbi]; bi++)
          {
            sum += geom->open_local_discrete_boundaries_correct_order_rank_0[dbi][bi];
            printf("%d:%d", bi, geom->open_local_discrete_boundaries_correct_order_rank_0[dbi][bi]);
        
            if (bi<geom->num_discrete_boundaries[dbi]-1)
            {
              printf(", ");
              if (bi%14 == 0 && bi!=0)
                printf("\n         ");
            }
          }
          printf(" | %d/%d\n", sum, geom->num_discrete_boundaries[dbi]);
        }
      printf("\n");
      }
    }
    
    for (di=0; di<geom->num_domains; di++)
    {
    
      sprintf(domain_groupname, "%s/%s", groupname, geom->domain_names[di]);
      domain_group_id = H5Gcreate(file_id, domain_groupname, H5P_DEFAULT, \
    				  H5P_DEFAULT, H5P_DEFAULT);
      for (si=0; si<species->num_species; si++)
      {
    	sprintf(species_groupname, "%s/%s", domain_groupname, species->species_names[si]);
    	species_group_id = H5Gcreate(file_id, species_groupname, H5P_DEFAULT, 
    				     H5P_DEFAULT, H5P_DEFAULT);
    	
    	// Write max value to file
    	write_h5_attr(comm, species_group_id, "max", H5REAL, 1,	\
    		      &max_values[di*species->num_species+si]);
    	
    	// Write min value to file
    	write_h5_attr(comm, species_group_id, "min", H5REAL, 1,		\
    		      &min_values[di*species->num_species+si]);
    	
    	// Write average value to file
    	REAL average = total_values[di*species->num_species+si]/geom->volumes[di];
    	write_h5_attr(comm, species_group_id, "average", H5REAL, 1,		\
    		      &average);
    
    	H5Gclose(species_group_id);
    	
      }
      H5Gclose(domain_group_id);
    }
    
    for (dbi=0; dbi<geom->num_global_discrete_boundaries; dbi++)
    {
      sprintf(datasetname, "%s/discrete_%s", groupname, \
              geom->boundary_names[geom->discrete_boundary_indices[dbi]]);
    
      hsize_t size[1] = {(hsize_t)geom->num_discrete_boundaries[dbi]};
      hid_t filespace = H5Screate_simple(1, size, NULL); 
      hid_t memspace =  H5Screate_simple(1, size, NULL); 
      hid_t plist_id = H5Pcreate(H5P_DATASET_CREATE);
      hid_t dset_id = H5Dcreate(file_id, datasetname, H5REAL, filespace, 
          		      H5P_DEFAULT, plist_id, H5P_DEFAULT);
      H5Pclose(plist_id);
      H5Sclose(filespace);
      filespace = H5Dget_space(dset_id);
      plist_id = H5Pcreate(H5P_DATASET_XFER);
    
      H5Dwrite(dset_id, H5T_STD_I32LE, filespace, filespace, plist_id, 
               geom->open_local_discrete_boundaries_correct_order_rank_0[dbi]);

      H5Sclose(filespace);
      H5Pclose(plist_id);
      H5Dclose(dset_id);
      H5Sclose(memspace);
    }

    // Clean upp communication buffer
    free(buff);

  }

  free(total_values);
  free(max_values);
  free(min_values);

}

//-----------------------------------------------------------------------------
void Species_init_fixed_domain_species(Species_t* species, domain_id num_fixed_domains, 
                                       domain_id* fixed_domains)
{

  unsigned int i, si, xi, yi, zi, di, xii, yii, zii;
  unsigned int i_geom_offset[NDIMS], i_offset[NDIMS];
  size_t voxel_ind;
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;
  size_t ind_fixed_single_species[species->num_species];
  memfill_size_t(ind_fixed_single_species, species->num_species, 0);

  // Iterate over fixed and collect the number of fixed voxels per species
  for (i=0; i<num_fixed_domains; i++)
  {
    
    // Get species index
    si = fixed_domains[2*i+1];

    // Iterate over the domains 
    for (xi=0; xi<geom->n[X]; xi++)
    {

      // Compute the X offsets 
      i_geom_offset[X] = xi*geom->n[Y]*geom->n[Z];

      // Iterate over the interior Y indices
      for (yi=0; yi<geom->n[Y]; yi++)
      {

        // Compute the Y offsets 
        i_geom_offset[Y] = yi*geom->n[Z];

        // Iterate over the interior Z indices
        for (zi=0; zi<geom->n[Z]; zi++)
        {
            
          // Get domain
          di = geom->domains[i_geom_offset[X]+i_geom_offset[Y]+zi];

          // If fixed domain increase the number of fixed voxels
          if (fixed_domains[2*i]==di)
            species->num_fixed_domain_species[si] += geom->subdivisions*geom->subdivisions* \
              geom->subdivisions;
        }
      }
    }
  }
  
  // Allocate memorty to hold index information for the fixed voxels
  for (si=0; si<species->num_species; si++)
    species->fixed_domain_species[si] = mpi_malloc(comm,                \
                     species->num_fixed_domain_species[si]*sizeof(size_t));

  // Iterate over given fixed domain species and store the voxel for each species
  for (i=0; i<num_fixed_domains; i++)
  {
    
    // Get species index
    si = fixed_domains[2*i+1]; 

    // Iterate over the domains 
    for (xi=0; xi<geom->n[X]; xi++)
    {

      // Compute the X offsets 
      i_geom_offset[X] = xi*geom->n[Y]*geom->n[Z];

      // Iterate over the interior Y indices
      for (yi=0; yi<geom->n[Y]; yi++)
      {

        // Compute the Y offsets 
        i_geom_offset[Y] = yi*geom->n[Z];

        // Iterate over the interior Z indices
        for (zi=0; zi<geom->n[Z]; zi++)
        {
            
          // Get domain
          di = geom->domains[i_geom_offset[X]+i_geom_offset[Y]+zi];

          // If domain is fixed for this species
          if (fixed_domains[2*i]==di)
          {
              
            // Iterate over all sub divisions
            for (xii=0; xii<geom->subdivisions; xii++)
            {
              
              // X offset in the species array
              i_offset[X] = (xi*geom->subdivisions+xii)*species->n[Z]*species->n[Y];

              for (yii=0; yii<geom->subdivisions; yii++)
              {

                // Y offset in the species array
                i_offset[Y] = (yi*geom->subdivisions+yii)*species->n[Z];

                for (zii=0; zii<geom->subdivisions; zii++)
                {
                  voxel_ind = i_offset[X]+i_offset[Y]+zi*geom->subdivisions+zii;
                  species->fixed_domain_species[si][\
                              ind_fixed_single_species[si]++] = voxel_ind;
                }
              }
            }
          }
        }
      }
    }
  }
}
//-----------------------------------------------------------------------------
void Species_output_time_stepping_information(Species_t* species, REAL tstop, 
					      size_t save_interval)
{
  unsigned int si, dsi;
  Geometry_t* geom = species->geom;
  MPI_Comm comm = geom->mpi_info.comm;

  mpi_printf0(comm, "\n");
  mpi_printf0(comm, "Time stepping:\n");
  mpi_printf0(comm, "-----------------------------------------------------------------------------\n");
  mpi_printf0(comm, "%13s %10.2g ms\n%13s%10.3g ms\n%13s%10zu\n%13s%10zu\n\n",
	      "tstop", tstop, "dt", species->dt, "num dt", 
	      (size_t)(tstop/species->dt), "save interval", save_interval);
  
  mpi_printf0(comm, "%13s%13s%10s\n", "Diffusion", "dt [ms]", "substeps");
  for (dsi=0; dsi<species->num_all_diffusive; dsi++)
  {
    si = species->all_diffusive[dsi];
    mpi_printf0(comm, "%13s%13.3g%10d\n", species->species_names[si], 
		species->species_sigma_dt[dsi],
		species->species_substeps[dsi]);
  }

  mpi_printf0(comm, "\n%13s%13s%10s\n", "", "dt [ms]", "substeps");
  mpi_printf0(comm, "%13s%13.3g%10d\n", "Discr bound", 
              species->stochastic_substep*species->dt,
              species->stochastic_substep);
  
  mpi_printf0(comm, "\n%13s%13s%10s\n", "", "dt [ms]", "substeps");
  mpi_printf0(comm, "%13s%13.3g%10d\n", "Reaction", 
              species->reaction_substep*species->dt,
              species->reaction_substep);
}
//-----------------------------------------------------------------------------
void Species_destruct(Species_t* species)
{
  unsigned int dim, dim2, dsi, di, si, bi;
  Geometry_t* geom = species->geom;

  for (di=0; di<geom->num_domains; di++)
  {

    if (species->init[di])
      free(species->init[di]);

    if (species->sigma[di])
      free(species->sigma[di]);
    
    if (species->diffusive[di])
      free(species->diffusive[di]);

    if (species->tot[di])
      free(species->tot[di]);

    if (species->k_on[di])
      free(species->k_on[di]);

    if (species->k_off[di])
      free(species->k_off[di]);

    if (species->bsp0[di])
      free(species->bsp0[di]);

    if (species->bsp1[di])
      free(species->bsp1[di]);

  }

  for (bi=0; bi<geom->num_boundaries; bi++)
  {
    free(species->boundary_voxels[bi]);
  }

  for (si=0; si<species->num_species; si++)
  {
    free(species->species_names[si]);
    free(species->u1[si]);
    free(species->du[si]);
    //_mm_free(species->du[si]);
    //_mm_free(species->du[si]);
    free(species->fixed_domain_species[si]);
  }
  
  for (dim2=0; dim2<NDIMS*2; dim2++)
  {

    for (dsi=0; dsi<species->num_all_diffusive; dsi++) 
    {
      if (species->ghost_values_receive[dim2][dsi])
        free(species->ghost_values_receive[dim2][dsi]);

      if (species->ghost_values_send[dim2][dsi])
        free(species->ghost_values_send[dim2][dsi]);
    }
    free(species->ghost_values_send[dim2]);
    free(species->ghost_values_receive[dim2]);

    if(species->ghost_alpha[dim2])
    {
      for (dsi=0; dsi<species->num_all_diffusive; dsi++) 
      {
    	if (species->ghost_alpha[dim2][dsi])
    	  free(species->ghost_alpha[dim2][dsi]);
      }
      free(species->ghost_alpha[dim2]);
    }
  }

  for (dim=0; dim<NDIMS; dim++)
  {
    if (species->alpha[dim])
    {
      for (dsi=0; dsi<species->num_all_diffusive; dsi++) 
      {
	if (species->alpha[dim][dsi])
	  free(species->alpha[dim][dsi]);
	  //_mm_free(species->alpha[dim][dsi]);
      }

      free(species->alpha[dim]);
    }
  }
  
  // Free scratch space for saving sheets
  if (species->u1_sheet_save)
    free(species->u1_sheet_save);

  // Free indices to save species
  if (species->ind_save_species)
    free(species->ind_save_species);

  // Free BoundaryFluxes
  BoundaryFluxes_destruct(species->boundary_fluxes);

  // FIXME: Add more clean ups
  free(species->fixed_domains);
  free(species->num_fixed_domain_species);
  free(species->num_boundary_voxels);
  free(species->boundary_voxels);
  free(species->local_domain_num);
  free(species->all_buffers);
  free(species->all_buffers_b);
  free(species->num_buffers);
  free(species->tot);
  free(species->k_on);
  free(species->k_off);
  free(species->bsp0);
  free(species->bsp1);
  free(species->u1);
  free(species->num_diffusive);
  free(species->diffusive);
  free(species->sigma);
  free(species->species_sigma_dt);
  free(species->species_substeps);
  free(species->init);
  free(species);
}
//-----------------------------------------------------------------------------
