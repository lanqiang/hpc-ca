#ifndef __OPTIONS_H
#define __OPTIONS_H

#include <stdlib.h>
#include <argp.h>
#include <stdint.h>

#include "types.h"

// Program documentation
const char* argp_program_version;
const char* argp_program_bug_address;
static char args_doc[];

// Used by main to communicate with parse_opt
struct arguments_
{

  // Simulation time
  double tstop;

  // Geometry resolution
  double h;

  // Output options
  int silent, verbose;

  // Output time
  REAL dt_save;

  // Random seed
  unsigned char seed_given;
  uint32_t seed;

  // Initialized RyRs
  unsigned int number_init_ryrs;
  int* open_ryrs;
  int max_ryr_ind;
  REAL dt_update_ryrs;
  REAL t_close_ryrs;

  // Update interval for reactions
  REAL dt_update_react;

  // Time step
  REAL dt;

  // List of species that should be saved
  char** species;
  int num_save_species;

  // List of z center points which should be used to save species in sheets
  int num_z_points;
  REAL* z_points;

  // File name options
  char *geometry_file;
  char *model_file;

  // A casename
  char *casename;

};

// Typedef for the argument
typedef struct arguments_ arguments_t;

// Parse a single option
error_t parse_opt (int key, char *arg, struct argp_state *state);

// Output parsed arguments
void arguments_output(MPI_Comm comm, arguments_t* arguments);

// Construct and initialize an arguments struct
arguments_t* arguments_construct(MPI_Comm comm);

// Destruct arguments struct
void arguments_destruct(arguments_t* arguments);


#endif
