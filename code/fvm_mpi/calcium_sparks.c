#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <mpi.h>

#include "geometry.h"
#include "species.h"
#include "boundaryfluxes.h"
#include "options.h"
#include "utils.h"

//#define __PAPI__

#ifdef __PAPI__
#include <papi.h>
#endif

//-----------------------------------------------------------------------------
static char args_doc[] =
  "calcium_spark -- A finite volume program modeling a calcium spark simulation";
//-----------------------------------------------------------------------------
static struct argp_option options[] = {
  {"verbose",        'v', 0,      0,  "Produce verbose output" },
  {"quiet",          'q', 0,      0,  "Don't produce any output" },
  {"tstop",          't', "TIME", 0,  "Run simulation for TIME number of ms. Default(10.)" },
  {"dt_save",        'o', "TIME", 0,  "Save solution for each dt_save. Default(.5)" },
  {"dt",             'd', "TIME", 0,  "Forced minimal timestep. If negative time step will be deducted from diffusion constants. Default(-1.)" },
  {"save_species",   's', "SPECIES", 0,  "A list of species that should be saved in 2D sheets at given z-coordinates." },
  {"save_z_coords",   'z', "COORD", 0,  "Z coordinates for the 2D sheet that will be saved." },
  {"resolution",     'h', "RES",  0,  "Run simulation with RES nm as the spatial resolution. Default(6.)" },
  {"species_file",   'm', "FILE", 0,  
   "Read the species model from FILE. Expects a .h5 file." },
  {"geometry_file",  'g', "FILE", 0,  
   "Read the geometry from FILE. Exprects a .h5 file." },
  {"casename",       'c', "CASENAME", 0,  
   "Sets the name of the run to CASENAME. Default (\"casename\")" },
  {"seed",           'S', "SEED", 0,  "Provide a seed for the random generator. If not given the seed will be random each run." },
  {"open_ryrs",      'R', "RyRs", 0,  "Initialize the given RyRs to open when starting simulation. Otherwise they are randomly initalized."},
  {"dt_update_ryrs",  'D', "TIME", 0,  "Time step for updating the RyRs. If negative it will be the same as global timestep. Default (-1)"},
  {"t_close_ryrs",    'C', "TIME", 0,  "Time for closing all RyRs. If negative RyRs are "
   "closed stochastically. Default (-1)"},
  {"dt_update_react",  'T', "TIME", 0,  "Time step for updating the reactions. If negative it will be the same as global timestep. Default (-1)"},
  { 0 }
};
//-----------------------------------------------------------------------------
static struct argp argp = { options, parse_opt, 0, args_doc };
//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
  push_time(SIM);
  size_t time_ind, save_interval;
  REAL t, dt;
  unsigned int output_ind;
  int ii;
  int nnx,nny,nnz;
  MPI_Init(&argc,&argv);
  MPI_Comm comm = MPI_COMM_WORLD;
 #ifdef __PAPI__
	int Events[]={PAPI_DP_OPS,PAPI_L1_DCM,PAPI_L2_DCM,PAPI_L3_DCM,PAPI_LD_INS,PAPI_SR_INS};
	int NUM_EVENTS = sizeof(Events)/sizeof(Events[0]);
	long long res_papi[NUM_EVENTS];
	char EventName[128];
	int num_hwcntrs = 0;
	int EventSet = PAPI_NULL;
	int retval;

	float real_time, proc_time, mflops;
	long long flpins;

	retval = PAPI_library_init(PAPI_VER_CURRENT);
	retval = PAPI_create_eventset(&EventSet);
	if(PAPI_add_events(EventSet, Events, NUM_EVENTS)!=PAPI_OK)
		printf("PAPI_add_events failed!\n");
	for( ii=0;ii<NUM_EVENTS;ii++)
		res_papi[ii] = 0;
 #endif



  // Init MPI
//  MPI_Init(&argc, &argv);
 // MPI_Comm comm = MPI_COMM_WORLD;

  // Check number of processes (could be made more flexible in the future)
  int size;
  MPI_Comm_size(comm, &size);
  
  // Check that the number of processes is a quadratic number
  if (fmod(sqrt((double)size),1.0) != 0.)
    mpi_printf_error(comm, "***Error: Expected a quadratic number for the number "\
                     "of processes: 0, 1, 4, 9, 16, 25 aso, got %d.\n", size);
    
  // Init the arguments with default values
  arguments_t* arguments = arguments_construct(comm);

  // Parse arguments
  argp_parse(&argp, argc, argv, 0, 0, arguments);
  
  if (!arguments->geometry_file)
    mpi_printf_error(comm, "*** ERROR: Expected a geometry file passed as argument:"\
                     " --geometry_file or -g\n");

  if (!arguments->model_file)
    mpi_printf_error(comm, "*** ERROR: Expected a species file passed as argument: "\
                     "--species_file or -m\n");

  // Force silent off if verbose is on
  arguments->silent = arguments->verbose==0 ? arguments->silent : 0;

  // Check for given seed
  if (arguments->seed_given)
    mpi_printf0(comm, "*** Warning: Seed not implemented.\n");

  // Output arguments
  arguments_output(comm, arguments);

  // Read in the geometry
  push_time(GEOM);
  Geometry_t* geom = Geometry_construct(comm, arguments);
  pop_time(GEOM);

  // Check given open RyR indices
  int num_ryr_boundaries = geom->boundary_size[Geometry_get_boundary_id(geom, "ryr")];
  if (arguments->max_ryr_ind >= num_ryr_boundaries)
    mpi_printf_error(comm, "*** ERROR: Expected max RyR indices given by option -R: "\
                     "%d, to be lower than the number of RyR boundaries: %d\n", 
                     arguments->max_ryr_ind, num_ryr_boundaries);

  // Read in model file
  push_time(SPECIES);
  Species_t* species = Species_construct(geom, arguments);
  pop_time(SPECIES);
  
  // Apply initial values
  Species_init_values(species);
  
  // Init discrete boundaries
  BoundaryFluxes_init_stochastic_boundaries(species, arguments);
  
  // Output species info
  output_ind = 0;
  dt = species->dt;
  save_interval = max_int(1, floor(arguments->dt_save/dt));

  if(arguments->verbose)
  {
    Species_output_init_values(species);
    BoundaryFluxes_output_init_data(species, arguments);
    Species_output_time_stepping_information(species, arguments->tstop, save_interval);
  }

  // Output init values to file
  Species_output_data(species, 0, 0.0, 0, arguments);


 #ifdef __PAPI__
/*	if(PAPI_start(EventSet)!=PAPI_OK)
		printf("PAPI_read_counters failed!\n");*/

/*	if((retval = PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
		printf("PAPI_flops failed!\n");*/
 #endif

  nnx = geom->n[0];
  nny = geom->n[1];
  nnz = geom->n[2];

  mpi_printf0(comm,"nnx = %d \n",nnx);
  mpi_printf0(comm,"nny = %d \n",nny);
  mpi_printf0(comm,"nnz = %d \n",nnz);

  // Time loop
  for (t=0., time_ind=1; t<arguments->tstop; t+=dt, time_ind++)
  {
    
#ifdef __PAPI__
      if(time_ind==1)
      {
      		if(PAPI_start(EventSet)!=PAPI_OK)
                        mpi_printf0(comm,"PAPI_read_counters failed!\n");
      
      		/*if((retval = PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                        mpi_printf0(comm,"PAPI_flops failed!\n");*/
      }
#endif

    // Do one diffusion step after communicating ghost values
    Species_step_diffusion(species, time_ind);

 #ifdef __PAPI__
	if(time_ind==1)
	{
      		if(PAPI_stop(EventSet,res_papi)!=PAPI_OK)
                       mpi_printf0(comm,"PAPI_accum_counters failed!\n");
                               for( ii=0;ii<NUM_EVENTS;ii++)
                                       {
                                               PAPI_event_code_to_name(Events[ii],EventName);
                                               mpi_printf0(comm,"PAPI Event name: %s, value: %lld \n",EventName, res_papi[ii]);
                                        }
	
 /*     	 	if((retval = PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                	mpi_printf0(comm,"PAPI_flops failed!\n");

        	mpi_printf0(comm,"Real_time: \t%f\nProc_time:\t%f\nTotal flpins:\t%lld\n MFLOPS:\t\t%f\n",real_time,proc_time,flpins,mflops);

        	PAPI_shutdown();*/
	}
 #endif

    
    // Evaluate and broadcast all stochastic events
    Species_evaluate_stochastic_events(species, time_ind);

    // Step all boundary fluxes
    Species_step_boundary_fluxes(species, time_ind);

    // Do one reaction step
    Species_step_reaction(species, time_ind);

    // Apply increment
    Species_apply_du(species);

    // If output
    if (time_ind % save_interval == 0)
      Species_output_data(species, ++output_ind, t+dt, time_ind, arguments);
      
  }


 #ifdef __PAPI__
/*	if(PAPI_stop(EventSet,res_papi)!=PAPI_OK)
		printf("PAPI_accum_counters failed!\n");
	for( ii=0;ii<NUM_EVENTS;ii++)
	{
		PAPI_event_code_to_name(Events[ii],EventName);
		printf("PAPI Event name: %s, value: %lld \n",EventName, res_papi[ii]);
	}*/

/*	if((retval = PAPI_flops(&real_time,&proc_time,&flpins,&mflops))<PAPI_OK)
                printf("PAPI_flops failed!\n");

	printf("Real_time: \t%f\nProc_time:\t%f\nTotal flpins:\t%lld\n MFLOPS:\t\t%f\n",real_time,proc_time,flpins,mflops);

	PAPI_shutdown();*/
 #endif


  // Output timings
  pop_time(SIM);
  if(arguments->verbose)
  {
    output_timings(comm);
  }

  // Cleanup
  Species_destruct(species);
  Geometry_destruct(geom);
  arguments_destruct(arguments);

  // Finalize MPI
  MPI_Finalize();

  return 0;
}
