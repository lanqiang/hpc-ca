#include "options.h"
#include "utils.h"

//-----------------------------------------------------------------------------
const char *argp_program_version =
  "calcium_spark 0.1";
//-----------------------------------------------------------------------------
const char *argp_program_bug_address =
  "<hake@simula.no>";
//-----------------------------------------------------------------------------
error_t parse_opt (int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  int offset, i;
  arguments_t *arguments = state->input;
  
  switch (key)
  {
  case 'c':
    arguments->casename = arg;
    break;

  case 'q': 
    arguments->silent = 1;
    break;

  case 'v':
    arguments->verbose = 1;
    break;

  case 'm':
    arguments->model_file = arg;
    break;

  case 'g':
    arguments->geometry_file = arg;
    break;

  case 't':
    arguments->tstop = atof(arg);
    break;

  case 'h':
    arguments->h = atof(arg);
    break;

  case 'o':
    arguments->dt_save = atof(arg);
    break;

  case 'd':
    arguments->dt = atof(arg);
    break;

  case 's':
    offset = 0;

    // Get where to start in argv
    while (state->argv[state->next+offset] != NULL &&
	   state->argv[state->next+offset][0] != '-')
      offset++;

    // Get species from argv
    arguments->species = &state->argv[state->next-1];
    arguments->num_save_species = offset+1;
    
    // Update next counter
    state->next += offset;
    break;

  case 'S':
    arguments->seed = atoi(arg);
    arguments->seed_given = 1;
    break;

  case 'T':
    arguments->dt_update_react = atof(arg);
    break;

  case 'D':
    arguments->dt_update_ryrs = atof(arg);
    break;
  
  case 'C':
    arguments->t_close_ryrs = atof(arg);
    break;
  
  case 'R':
    offset = 0;

    // Get where to start in argv
    while (state->argv[state->next+offset] != NULL &&
	   state->argv[state->next+offset][0] != '-')
      offset++;

    // Get open RyRs from argv
    arguments->number_init_ryrs = offset + 1;
    arguments->open_ryrs = malloc(sizeof(int)*arguments->number_init_ryrs);
    
    for (i=0; i<arguments->number_init_ryrs; i++)
    {
      arguments->open_ryrs[i] = atoi(state->argv[state->next-1+i]);
      
      if (arguments->open_ryrs[i]<0)
        mpi_printf_error(MPI_COMM_WORLD, "***Error: Expected a positive number for"\
                         " the open RyR indices given by option -R, got %d.\n", 
                         arguments->open_ryrs[i]);
      
      arguments->max_ryr_ind = max_int(arguments->max_ryr_ind, arguments->open_ryrs[i]);

      if (i > 0 && arguments->open_ryrs[i] <= arguments->open_ryrs[i-1])
        mpi_printf_error(MPI_COMM_WORLD, "***Error: Expected a monotonicly increasing"\
                         " numbers for the open RyR indices given by option -R, "\
                         "got %d and %d.\n", arguments->open_ryrs[i-1], 
                         arguments->open_ryrs[i]);
    }
    
    // Update next counter
    state->next += offset;
    break;
    
  case 'z':
    offset = 0;

    // Get where to start in argv
    while (state->argv[state->next+offset] != NULL &&
	   state->argv[state->next+offset][0] != '-')
      offset++;

    // Get open RyRs from argv
    arguments->num_z_points = offset + 1;
    arguments->z_points = malloc(sizeof(REAL)*arguments->num_z_points);
    
    for (i=0; i<arguments->num_z_points; i++)
      arguments->z_points[i] = atof(state->argv[state->next-1+i]);
    
    // Update next counter
    state->next += offset;
    break;
    
  case ARGP_KEY_ARG:
    
    if (state->arg_num >= 0)
      /* Too many arguments. */
      argp_usage (state);
    break;
     
  default:
    return ARGP_ERR_UNKNOWN;
  
  }
  
  return 0;
}
//-----------------------------------------------------------------------------
void arguments_output(MPI_Comm comm, arguments_t* arguments)
{
  
  if (!arguments->silent)
  {
    unsigned int i;
    mpi_printf0(comm, "\n");
    mpi_printf0(comm, "Arguments:\n");
    mpi_printf0(comm, "-----------------------------------------------------------------------------\n");
    mpi_printf0(comm, "  casename:        \"%s\"\n", arguments->casename);
    mpi_printf0(comm, "  geometry_file:   \"%s\"\n", arguments->geometry_file);
    mpi_printf0(comm, "  species_file:    \"%s\"\n", arguments->model_file);
    mpi_printf0(comm, "  tstop:           %.6f ms\n", arguments->tstop);
    mpi_printf0(comm, "  h:               %.1f nm\n", arguments->h);
    mpi_printf0(comm, "  dt     :         %.3g\n", arguments->dt);
    mpi_printf0(comm, "  dt_save:         %.3g\n", arguments->dt_save);
    mpi_printf0(comm, "  dt_update_ryrs:  %.3g\n", arguments->dt_update_ryrs);
    mpi_printf0(comm, "  t_close_ryrs:    %.3g\n", arguments->t_close_ryrs);
    mpi_printf0(comm, "  dt_update_react: %.3g\n", arguments->dt_update_react);
    mpi_printf0(comm, "  save_2D_species: %s", arguments->num_save_species ? "" : "-");
    for (i=0; i<arguments->num_save_species; i++)
    {
      mpi_printf0(comm, "%s", arguments->species[i]);
      if (i<arguments->num_save_species-1)
	mpi_printf0(comm, ", ");
    }
    mpi_printf0(comm, "\n");
    if (arguments->num_save_species)
    {
      mpi_printf0(comm, "    z-coords (nm): ");

      // If no z coord was given we choose default
      if (arguments->num_z_points==1 && arguments->z_points == NULL)
      {
        mpi_printf0(comm, "center");
      }
      else
      {
        for (i=0; i<arguments->num_z_points; i++)
        {
          mpi_printf0(comm, "%.2f", arguments->z_points[i]);
          if (i<arguments->num_z_points-1)
            mpi_printf0(comm, ", ");
        }
      }
      mpi_printf0(comm, "\n");
    }

    mpi_printf0(comm, "  open RyRs:       %s", arguments->number_init_ryrs ? "" : "-");
    for (i=0; i<arguments->number_init_ryrs; i++)
    {
      mpi_printf0(comm, "%d", arguments->open_ryrs[i]);
      if (i<arguments->number_init_ryrs-1)
	mpi_printf0(comm, ", ");
    }
    
    mpi_printf0(comm, "\n\n");
    fflush(stdout);
    MPI_Barrier(comm);
  }
  
}
//-----------------------------------------------------------------------------
arguments_t* arguments_construct(MPI_Comm comm)
{
  // Our arguments with default values
  arguments_t* arguments = mpi_malloc(comm, sizeof(arguments_t));
  arguments->geometry_file = NULL;
  arguments->model_file = NULL;
  arguments->casename = "casename";
  arguments->silent = 0;
  arguments->verbose = 0;
  arguments->tstop = 10.;
  arguments->h = 6.;
  arguments->dt_save = .5;
  arguments->dt = -1.;
  arguments->num_save_species = 0;
  arguments->num_z_points = 1;
  arguments->z_points = NULL;
  arguments->number_init_ryrs = 0;
  arguments->dt_update_react = -1.;
  arguments->open_ryrs = NULL;
  arguments->dt_update_ryrs = -1.;
  arguments->t_close_ryrs = -1.;
  arguments->max_ryr_ind = -1;
  arguments->seed_given = 0;
  arguments->seed = 0;
  
  return arguments;
}
//-----------------------------------------------------------------------------
void arguments_destruct(arguments_t* arguments)
{
  free(arguments->open_ryrs);
  free(arguments);
}
//-----------------------------------------------------------------------------
