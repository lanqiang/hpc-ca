%------------------------------------------------------------------------------
\documentclass[letterpaper,10pt]{article}
%------------------------------------------------------------------------------

% Use round as parameter for rounded parenthesis in the text
\usepackage[sort&compress,comma]{natbib} 
%\renewcommand{\bibnumfmt}[1]{#1.}

\usepackage[utf8]{inputenc}
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc}
\usepackage{textcomp}

% Uncomment when doing final
\usepackage[protrusion=true, expansion=true]{microtype} 
\usepackage{fancyhdr}                % flexible headings
\usepackage{amsmath}                 % powerful AMS LaTeX 
\usepackage{amssymb}
\usepackage{graphicx}                % include graphics
\usepackage{url}                     % Enable the use of nice formating of urls usin
\usepackage[colorlinks=true, linkcolor=black, citecolor=black, 
filecolor=black, urlcolor=black, pdfmenubar=true, pdftoolbar=true, 
bookmarks=true]{hyperref}

%\input{mathcmdsrtf}
\input{mathcmds}
\hyphenation{}

\usepackage{color}
\newcommand{\Johan}[1]{{\textcolor{red}{\textbf{#1}}}}
\newcommand{\Johanrevised}[1]{#1}
\newcommand{\Andyrevised}[1]{#1}
\newcommand{\Andy}[1]{#1}
\newcommand{\Pete}[1]{{\textcolor{blue}{\textbf{#1}}}}
\renewcommand{\cite}[1]{\citep{#1}}
\newcommand{\usefigures}[1]{#1}
%\newcommand{\usefigures}[1]{\relax}

% Letter size 215.9 x 279.4 mm (8.5 x 11)
% A4 size 210 x 297 mm
\renewcommand{\baselinestretch}{1.5}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}
\setlength{\textwidth}{6.5in}%{159.2mm}
\setlength{\textheight}{9in}%{246.2mm}
\setlength{\headheight}{0mm}
\setlength{\headsep}{0mm}
\setlength{\topmargin}{-4mm}
%\setlength{\marginparwidth}{30mm}

%------------------------------------------------------------------------------
\title{Multiscale mode of subcellular \Ca diffusion}
%------------------------------------------------------------------------------
\author{%
  Johan Hake$^{\scriptscriptstyle 1}$, Glenn T Lines$^{\scriptscriptstyle 1}$, Xing Cai$^{\scriptscriptstyle 1}$\\
  \small{$^1$ Simula Research Laboratory, Center for Biomedical
    Computing, Lysaker, Norway}}
%------------------------------------------------------------------------------
\date{}
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
\begin{document}
%------------------------------------------------------------------------------
\maketitle
%------------------------------------------------------------------------------
%\clearpage
\noindent\\
%------------------------------------------------------------------------------
\begin{abstract}
Coming!
%  Triggered release of \Ca from an individual sarcoplasmic reticulum
%  (SR) \Ca release unit (CRU) is the fundamental event of cardiac
%  excitation-contraction coupling, and spontaneous release events
%  (sparks) are the major contributor to diastolic \Ca leak in
%  cardiomyocytes. Previous model studies have predicted that the
%  duration and magnitude of the spark is determined by the local CRU
%  geometry, as well as the localization and density of \Ca handling
%  proteins. We have created a detailed computational model of a CRU,
%  and developed novel tools to generate the computational geometry
%  from electron tomographic images. \Ca diffusion was modeled within
%  the SR and the cytosol to examine the effects of localization and
%  density of the \Na/\Ca exchanger, Sarco/endoplasmic reticulum \Ca
%  ATPase 2 (SERCA), and calsequestrin on spark dynamics. We reconcile
%  previous model predictions of approximately 90 \% local \Ca
%  depletion in junctional SR, with experimental reports of about 40
%  \%. This analysis supports the hypothesis that dye kinetics and
%  optical averaging effects can have a significant impact on measures
%  of spark dynamics. Our model also predicts that distributing
%  calsequestrin within non-junctional Z-disc SR compartments, in
%  addition to the junctional compartment, prolongs spark release time
%  as reported by Fluo5. By pumping \Ca back into the SR during a
%  release, SERCA is able to prolong a \Ca spark, and this may
%  contribute to SERCA-dependent changes in \Ca-wave speed. Finally, we
%  show that including the \Na/\Ca exchanger inside the dyadic cleft
%  does not alter local \CaC during a spark.

\end{abstract}
%------------------------------------------------------------------------------
\noindent\textbf{Abbreviations:} \\CICR, \Ca induced \Ca release; CRU, \Ca
release unit; CMDN, calmodulin; CSQN, calsequestrin; EC,
excitation-contraction; jSR, junctional sarcoplasmic reticulum; NCX,
\Na/\Ca exchanger; ODE, ordinary differential equations; PDEs, partial
differential equations; RyR, ryanodine receptors; SERCA,
Sarco/endoplasmic reticulum \Ca-ATPase 2; SR, sarcoplasmic reticulum;
TRPN, troponin C
%------------------------------------------------------------------------------
\clearpage

%------------------------------------------------------------------------------
\section{Introduction}

\Ca release are facilitated at \Ca release units (CRU)s, which are
mostly positioned at z-lines. The z-lines coincides with the endpoints
of the sarcomeres. See Fig. \ref{fig:scetch} for a detailed sketch of
a sarcomere and a CRU. A myocyte typically has some 20 000 CRU. During
normal circumstances are \Ca released \Ca from the sarcoplasmic
reticulum (SR) simultaneously from all CRUs. This release is
triggered by the arrival of an action potential (AP)
\cite{Bers_2001_book}. However the main \Ca release channel, the
Ryanodine receptor can spontaneously open and trigger local \Ca
release without a triggering AP. During pathological circumstances,
such as heart failure, this probability increases. If it gets high
enough it can trigger \Ca waves within the cell. These waves can be
arrhythmogenic.
\begin{figure}
  \center
  \includegraphics[width=15cm]{figures/Ca-handling-no-Ca}
  \caption{\textbf{Schematic representation of a Sarcomere (left) and
      a \Ca release unit (CRU, right).} The sarcomere is defined by
    the region between two Z-lines. At the z-lines t-tubules and
    Sarcoplasmic reticulum (SR) form CRU which are geometric and
    functional structures. Inside SR \Ca concentration is
    high. Ryanodine receptors (RyRs) positioned inside the CRU
    facilitate \Ca release. SERCA pumps bring \Ca back into
    SR. (Colored from \citet{Fawc_1969_1}.)}
  \label{fig:scetch}
\end{figure}

%------------------------------------------------------------------------------
\section{Methods}

\subsection{Geometry}
\begin{figure}
  \center
  \includegraphics[width=10cm]{figures/myocytemodel}
  \caption{\textbf{Sketch of computational geometry.} \textbf{A)} The
    dimensions of a whole cardiomyocyte is approximately
    100$\mu$m$\times$10$\mu$m$\times$10$\mu$m. \textbf{B)} Our
    geometrical model consists of $\mathrm{N_{Sar}}$ sarcomeres, which
    forms our domain: $\Omega$. \textbf{C, D}, Each sarcomere has a
    number of CRUs and RyRs. The number and distribution of these are
    determined by a growth algorithm, see text. The whole domain is
    discretized using a global discretisation parameter, dx.}
  \label{fig:scetch}
\end{figure}

The length of each sarcomere is 2 $\mu$. We will investigate the
effect of distributing the CRU at different distances. Smaller
distances would for example imply that Ca waves spread more
easily. The size of each CRU is determined by the length of the side
of the squre defining the $i$th CRU, $\mathrm{dx_{CRU}^i}$.

The CRU and RyRs are randomly distributed along the Z-lines. We have
employed a modified version of the simple growth algortithm presented
by \citet{Badd_2009_22275} to generate the CRUs. On a 2D lattice with
with 30 nm spacing each pixel can be either empty or contain a RyR. At
start all pixels are empty. There are three probabilities that govern
the CRU generation. \textit{i)} $P_{nucleation}$, \textit{ii)}
$P_{growth}$ and \textit{iii)} $P_{retention}$. The sum of
$P_{nucleation}$ and $N\;P_{growth}$ determines the probability for an
empty pixel become a RyR, and $P_{retention}$ determines the
probability for a RyR to retain. Here $N$ is the number of neighboring
pixels (diagonal neighbors included) that contain a RyR. The algorithm
is run for 150 iterations. CSQN is distributed within the SR
compartment at the pixels that contains a RyR and its neighbors
(diagonal included). In Figure \ref{fig:cluster_distribution} we
present the result of two runs: \textit{i)} the upper row represent a
healthy myocyte and \textit{ii)} the lower row, which represents a
failing myocyte. For both runs we have used $P_{growth}$ = 0.014. For
the healthy myocyte we have used $P_{nucleation} = 0.5\times 10^{-4}$
and $P_{retention} = 0.937$ and for the failing myocyte we used
$P_{nucleation} = 2.5\times 10^{-4}$ and $P_{retention} = 0.915$.

Note that the number of mesh voxels a RyR occupies is limited to 1,
regardless of the mesh resolution $dx$, whereas the number of mesh
voxels the CSQN occupy increases when $dx$ is reduced.

\begin{figure}
  \center
  \includegraphics[width=10cm]{figures/cluster_distribution.pdf}
  \caption{\textbf{There are more but smaller \Ca release units (CRUs)
      in failing myocytes.} In the upper row RyR distribution (left)
    and CSQN distribution (right) is presented for a healthy
    myocyte. The lower row presents the same data but for a failing
    myocyte.}
  \label{fig:cluster_distribution}
\end{figure}


\subsection{Mathematical model}
\label{sec:mesh}

The \Ca dynamics are described by a system of partial differential
reaction-diffusion equations. All species with their initial values
are described in Table \ref{tab:initial-values}. \Johan{Need species
  related updates!}

The system of partial differential equations (PDE)s is described by
the following set of equations:
\renewcommand{\arraystretch}{1.3}
\begin{equation}
  \left.
  \begin{array}{r@{\quad=\quad}l}
  \diff[c]{t} & \mathrm{D^{cyt}_{Ca}}\nabla^2c - \sum_i R_i(c, cB_i),\\
  \diff[c^{sr}]{t} & \mathrm{D^{sr}_{Ca}}\nabla^2c^{sr} - \sum_i R_i(c^{sr}, cB^{sr}_i),\\
  \diff[cB_i]{t} & \mathrm{D}^{\mathrm{cyt}}_{i}\nabla^2cB_i + R_i(c, cB_i),\\
  R_i(c, cB_i) & \mathrm{k}^i_{\mathrm{on}}c(\mathrm{B}^i_{\mathrm{tot}}-cB_i)-\mathrm{k}^i_{\mathrm{off}}cB_i,
  \end{array}
\right\} x \in \Omega
\end{equation}
with boundary conditions:
\begin{equation}
  \left.
  \begin{array}{r@{\quad=\quad}l}
    \diff[c]{n} & 0, \\
    \diff[c^{sr}]{n} & 0, \\
    \diff[cB_i]{n} & 0.
\end{array}
\right\} x \in \partial\Omega.
\end{equation}
\renewcommand{\arraystretch}{1.0} Here $c$ is the \CaC in cytosol and
$c^{sr}$ is the \CaC in SR and D$\mathrm{^{cyt}_{Ca}}$ and
D$\mathrm{^{sr}_{Ca}}$ are respectively the diffusion constants of \Ca
in cytosol and SR. $cB_i$ is the concentration and
D$^{\mathrm{cyt}}_i$ the diffusion constant of the $i$th buffer with
\Ca bound to it. $i$ refers to ATP, CMDN (calmodulin), Fluo4 or TRPN
(troponin C). See Table~\ref{tab:species} for values of the
parameters, including references, for all included species \Johan{Need
  species related updates!}.

\subsubsection*{Ryanodine receptor dynamics}
The dynamics of the \Ca RyR is stochastically controlled by a 4 state
Markov model \cite{Ster_1999_469}. The states and the rates between
them are described in Fig. \ref{fig:RyR_mm}. The rate variables are
described by following equations:
\begin{equation}
  \label{eq:RyR_rates}
  \alpha=\alpha_0\,c^2;\;\beta=\beta_0;\gamma=\gamma_0\,c;\delta=\delta_0.
\end{equation}
Here $c$ is the local \CaC and $\alpha_0$, $\beta_0$, $\gamma_0$ and
$\delta_0$ are parameters chosen so the model behavior fit macroscopic
measurements such as spark frequency, spark duration and spark
restituion. \Johan{More and/or less data?} \Johan{Have to be done!} 

The state transitions are govern stochastically as a Poisson
process. The probability for a transition to take place within a time
step \Dt is given by:
\begin{equation}
  \label{eq:prob}
  P(\Dt, s) = 1-\e^{-\Dt\,r(s)}.
\end{equation}
Here $s$ is the present state the RyR is in and $r(s)$ is a function
of the transition rates from Eq.~(\ref{eq:RyR_rates}). If \Dt is
sufficiently small Eq.~(\ref{eq:prob}) reduces to:
\begin{equation}
  \label{eq:prob_reduced}
  P(\Dt, s) \simeq \Dt\,r(s)
\end{equation}
For each time step and RyR a quasi random number $w$ is generated and
compared with $P$. If the number is smaller a state transition has
occured.

When the $n$th RyR opens it creates a flux, $J^n_{RyR}$ that transport
\Ca from SR to cytosol. The flux is described by:
\begin{equation}
  \label{eq:J_RyR}
  J^n_{RyR} = \left\{
    \begin{array}{r@{\quad:\quad}l}
      D_{RyR}\left(c^{sr}-c\right) & s^n = 1 \\
      0 & s^n \in [0,2,3]
    \end{array}\right.
\end{equation}
Here $D_{RyR}$ determines the conductancy of a RyR channel and it is
fitted so the current through a single RyR equals 0.5 pA
\cite{Kett_2003_407, Gill_2008_3706}. $s^n$ is the value of the $n$th
RyR state variable, see Fig. \ref{fig:RyR_mm}.

\begin{figure}
  \centering
  \includegraphics[width=6cm]{figures/RyR_mm}
  \caption{\textbf{Four state Markov model describing the dynamics of
      the RyR.} Each RyR can be in either one of these 4 states. If a
    RyR is in state 1 it is open and a flux between SR and Cytosol is
    generated.}
  \label{fig:RyR_mm}
\end{figure}

\subsubsection*{Sarco/endoplasmic reticulum \Ca ATP-ase flux}

SERCA flux was described by a simplified version of the two state
SERCA model developed by \cite{Tran_2009_2029}. The simplifications
were made to reduce computation, but did not significantly alter the
turnover rate. Our model differs from the original by 4 \%, at most,
over the relevant ranges of $c_{\mathrm{cyt}}$ and $c_{\mathrm{sr}}$,
data not shown. The simplified SERCA model is given by:
\begin{eqnarray}
  \alpha_1^+ &=& \frac{k^+_2\,\mathrm{Mg\tilde{A}TP\,\tilde{C}a_i^2}}{\mathrm{\tilde{H}_i(1+\tilde{M}gATP(1+\tilde{H}_1))}}\label{eq:alpha1}\\
  \alpha_2^+ &=& \frac{k^+_3\,\mathrm{\tilde{H}_{SR}}}{\mathrm{\tilde{H}_{SR}(1+\tilde{H})+\tilde{H}}}\label{eq:alpha2}\\
  \alpha_1^- &=& \frac{k^-_2\,\mathrm{[MgADP]\,\tilde{C}a^2_{SR}\,\tilde{H}}}{\mathrm{\tilde{H}_{SR}(1+\tilde{H})+\tilde{H}}}\label{eq:alpha3}\\
  \alpha_2^- &=& \frac{k^-_3\,\mathrm{[Pi]\tilde{H}_i}}{\mathrm{\tilde{H}_i(1+\tilde{M}gATP(1+\tilde{H}_1))}}\label{eq:alpha4}\\
  v_{\mathrm{cycle}} &=& \frac{\alpha_1^+\alpha_2^+ - \alpha_1^-\alpha_2^-}{\alpha_1^++\alpha_2^++\alpha_1^-+\alpha_2^-}\\
  J_{\mathrm{\scriptscriptstyle SERCA}}&=& 2\,\mathrm{s_{\scriptscriptstyle SERCA}}\,\rho_{\mathrm{\scriptscriptstyle SERCA}}\,\mathrm{V_{cyt}toA_{sr}}\,v_{\mathrm{cycle}}
\end{eqnarray}
Here all parameters described in
Eq.~(\ref{eq:alpha1})~-(\ref{eq:alpha4}) are given in
 \cite{Tran_2009_2029}, with the exception that we have changed the
temperature to 22\deg{}C. $\mathrm{V_{cyt}toA_{sr}}$ is the cytosolic
volume to SR area ratio and is computed by:
\begin{equation}
  \mathrm{V_{cyt}toA_{sr}} = \mathrm{\frac{V^{frac}_{cyt}}{A_{jsr}toV_{cell}\frac{V^{frac}_{nsr}+V^{frac}_{jsr}}{V^{frac}_{jsr}}}}.
\label{eq:cyt-volume-to-sr-area}
\end{equation}
The value of $\mathrm{V_{cyt}toA_{sr}}$ {was} 307 nm, which is
close to the measured value from the generated geometry: 346 nm. This
results in 1.3 SERCA pumps per 100 nm$^2$, which is reasonable based
on the packing limit imposed by the size of the SERCA protein, which
is \~ 4-6 nm across depending on what state it is in
\cite{Toyo_2008_3}. $\mathrm{s_{\scriptscriptstyle SERCA}}$ is a
scaling parameter, which was fitted such that the refill time constant
after a release, as measured by Fluo5, corresponded to the
experimentally measured value of 160 ms \cite{Zima_2008_105}. The
fitted values for $\mathrm{s_{\scriptscriptstyle SERCA}}$ were 1.5 for
CSQN distributed in Z-line SR, and 2.5 for CSQN concentrated in
jSR. Note that a value of 1 implies no scaling, and corresponds to a
SERCA density determined only by the geometry, the SERCA model, the
volume density, the computed cytosolic volume to SR area ratio
($\mathrm{V_{cyt}toA_{sr}}$), and the experimentally measured refill
rate. As such, the fitted scale parameter can be seen as a free
parameter, which scales one or several of the fixed
parameters. Considering the number of fixed parameters and some
uncertainty surrounding their ascribed values, 1.5 and 2.5 are
remarkably close to 1. The parameter values used for the SERCA flux,
which are not described in \citet{Tran_2009_2029}, are given in Table
3{ in the supplementary methods section}.

%Labeling studies suggest that SERCA is not distributed evenly
%throughout the sarcomere \cite{Bidw_2011_35044}, but more concentrated
%at the Z-line. Where SERCA is included in our model, we
%distribute{d} it evenly at the SR membranes. However, because the
%Z-line SR compartments are much larger, see
%Fig.~\ref{fig:geometry}\textbf{F}, the apparent concentration of the
%SERCA will be larger at the Z-line.
%
%We hypothesized that incorporating the SERCA pump within the CRU
%domain will prolong release. During the release \Ca is pumped back
%into SR, thereby delaying the depletion of \Ca in the jSR, which in
%our model, will prolong release. To test if this was the case we
%compared release termination for different SERCA distributions:
%\textit{(i)} without any SERCA, \textit{(ii)} with SERCA applied to
%all SR boundaries with the exception of the jSR compartment, and
%\textit{(iii)} SERCA applied to the boundaries in \textit{(ii)}
%together with the Back boundary of the jSR.
%
%We also measured how much \Ca could be pumped into the SR via SERCA
%from a \Ca wave-front arriving at the CRU. This measurement is a
%rudimentary proof-of-principle test for how effectively a \Ca
%wave-front can sensitize the RyRs in the CRU via increased SR \Ca load
% \cite{Kell_2007_39}. The distance between the CRUs in the longitudinal
%direction {was} \~ 2 $\mu$m and with a \Ca wave traveling at 100
%$\mu$m s$^{-1}$ \cite{Lamo_1998_669, Ter_2007_457}, each CRU should
%recruit the next at roughly 20 ms intervals. Therefore we fixed the
%\Ca in the cytosolic domain to values representative of an arriving
%\Ca wave-front, and measured the \Ca level in the jSR over 20 ms.

%\subsubsection*{T-tubule fluxes}
%
%At the t-tubule boundaries we include three different fluxes. The NCX,
%{the} sarcolemmal \Ca pump (pCa) and a background \Ca flux
%(Cab). {Detailed descriptions of the equations used to model these
%three fluxes are given in the supplementary methods section.}
%
%Localization of NCX is likely to be very important to its role in
%myocardial \Ca handling, particularly if as previously suggested
% \cite{Traf_1995_577, Line_2006_779}, NCX exists within the dyad where
%it would experience a very high local \CaC. We
%hypothesized that including NCX flux within the dyad would be capable
%of directly and markedly altering dyadic \CaC, and spark dynamics. We
%tested this hypothesis by comparing simulations for which the t-tubule
%fluxes were either included or excluded from the dyad.
%
%\subsection{Linescan generation}
%
%To compare the size of the generated spark to experimental
%observations we computed a linescan image based on the F/F0 signal of
%the included Fluo4. {Because} the generated geometry is too small
%to be used directly in this calculation, we applied the method
%previously described by {Smith $et$ $al$.,} \cite{Smit_1998_15}
%to {reconstruct} a linescan image of the spark. This was done by
%recording the release current from a full simulation of our model, and
%applying this as a point source to the PDE, as described
%{above}. {Owing} to radial symmetry the problem is
%{effectively one}-dimensional. The resulting F/F0 signal {was}
%then integrated at the linescan integration points, using a Gaussian
%kernel, {with previously reported parameters}
% \cite{Smit_1998_15}. White noise is added to the resulting image to
%mimic experimental conditions.


\renewcommand{\arraystretch}{1.0}
\begin{table}\scriptsize%\footnotesize
\center
\caption{\textbf{Species parameters}}
\label{tab:species}
\begin{tabular}{|l|l|r|r|}
\hline
\textbf{Parameter}&\textbf{Description}&\textbf{Value}&\textbf{Ref}\\
\hline
D$^{\mathrm{cyt}}_{\mathrm{Ca}}$ & Diffusion constant of \Ca in the cytosolic domain & $220\times 10 ^{3}$ nm$^2$ms$^{-1}$& \cite{Louc_2010_1377}\\
\hline
D$^{\mathrm{sr}}_{\mathrm{Ca}}$ & Diffusion constant of \Ca in the SR compartments & $73.3\times 10 ^{3}$ nm$^2$ms$^{-1}$$\P$& \\
\hline
D$^{\mathrm{cyt}}_{\mathrm{ATP}}$ & Diffusion constant of ATP in Cytosol & $140\times 10 ^{3}$ nm$^2$ms$^{-1}$& \cite{Vale_2007_155}\\
\hline
$\mathrm{B^{ATP}_{tot}}$ & Total ATP concentration & 455 $\mu$M$^{\star}$& \cite{Bers_2001_book}\\
\hline
$\mathrm{k^{ATP}_{off}}$ & ATP Ca$^{2+}$ off rate & 45 ms$^{-1}$& \cite{Pich_2011_847}\\
\hline
$\mathrm{k^{ATP}_{on}}$ & ATP Ca$^{2+}$ on rate & $225\times 10 ^{-3}$ ms$^{-1}\,\mu$M$^{-1}$& \cite{Bers_2001_book,Pich_2011_847}\\
\hline
$\mathrm{Kd^{ATP}}$ & ATP Ca$^{2+}$ dissociation constant & 200 $\mu$M& \cite{Bers_2001_book}\\
\hline
D$^{\mathrm{cyt}}_{\mathrm{CMDN}}$ & Diffusion constant of Calmodulin in Cytosol & $25\times 10 ^{3}$ nm$^2$ms$^{-1}$& \cite{Mich_2002_3134}\\
\hline
$\mathrm{B^{CMDN}_{tot}}$ & Total Calmodulin concentration & 24 $\mu$M& \cite{Fabi_1983_C1}\\
\hline
$\mathrm{k^{CMDN}_{off}}$ & Calmodulin Ca$^{2+}$ off rate & $238\times 10 ^{-3}$ ms$^{-1}$& \cite{Robe_1981_559}\\
\hline
$\mathrm{k^{CMDN}_{on}}$ & Calmodulin Ca$^{2+}$ on rate & $34\times 10 ^{-3}$ ms$^{-1}\,\mu$M$^{-1}$& \cite{Pich_2011_847,Robe_1981_559}\\
\hline
$\mathrm{Kd^{CMDN}}$ & Calmodulin Ca$^{2+}$ dissociation constant & 7 $\mu$M& \cite{Pich_2011_847}\\
\hline
$\mathrm{B^{CSQN}_{tot}}$ & Total Calsequestrin concentration (only in jSR) & $30\times 10 ^{3}$ $\mu$M$^{\dagger}$& \cite{Bers_2001_book}\\
\hline
$\mathrm{B^{CSQN}_{tot}}$ & Total Calsequestrin concentration (in all Z-line domain) & $6.39\times 10 ^{3}$ $\mu$M$^{\ddagger}$& \cite{Bers_2001_book}\\
\hline
$\mathrm{k^{CSQN}_{off}}$ & Calsequestrin Ca$^{2+}$ off rate & 65 ms$^{-1}$& \cite{Pich_2011_847}\\
\hline
$\mathrm{k^{CSQN}_{on}}$ & Calsequestrin Ca$^{2+}$ on rate & $102\times 10 ^{-3}$ ms$^{-1}\,\mu$M$^{-1}$& \cite{Pich_2011_847,Shan_1997_1524}\\
\hline
$\mathrm{Kd^{CSQN}}$ & Calsequestrin Ca$^{2+}$ dissociation constant & 640 $\mu$M& \cite{Shan_1997_1524}\\
\hline
D$^{\mathrm{cyt}}_{\mathrm{Fluo}}$ & Diffusion constant of Fluo4 in Cytosol & $42\times 10 ^{3}$ nm$^2$ms$^{-1}$& \cite{Pich_2011_847}\\
\hline
$\mathrm{B^{Fluo}_{tot}}$ & Total Fluo4 concentration & 25 $\mu$M& \cite{Pich_2011_847}\\
\hline
$\mathrm{k^{Fluo}_{off}}$ & Fluo4 Ca$^{2+}$ off rate & $110\times 10 ^{-3}$ ms$^{-1}$& \cite{Pich_2011_847}\\
\hline
$\mathrm{k^{Fluo}_{on}}$ & Fluo4 Ca$^{2+}$ on rate & $100\times 10 ^{-3}$ ms$^{-1}\,\mu$M$^{-1}$& \cite{Pich_2011_847}\\
\hline
$\mathrm{Kd^{Fluo}}$ & Fluo4 Ca$^{2+}$ dissociation constant & 1.1 $\mu$M& \cite{Pich_2011_847}\\
\hline
D$^{\mathrm{sr}}_{\mathrm{Fluo5}}$ & Diffusion constant of Fluo5 in SR & $8\times 10 ^{3}$ nm$^2$ms$^{-1}$& \cite{Pich_2011_847}\\
\hline
$\mathrm{B^{Fluo5}_{tot}}$ & Total Fluo5 concentration & 25 $\mu$M& \cite{Pich_2011_847}\\
\hline
$\mathrm{k^{Fluo5}_{off}}$ & Fluo5 Ca$^{2+}$ off rate & $100\times 10 ^{-3}$ ms$^{-1}$$^{\S}$& \\
\hline
$\mathrm{k^{Fluo5}_{on}}$ & Fluo5 Ca$^{2+}$ on rate & $250\times 10 ^{-6}$ ms$^{-1}\,\mu$M$^{-1}$& \cite{Pich_2011_847}\\
\hline
$\mathrm{Kd^{Fluo5}}$ & Fluo5 Ca$^{2+}$ dissociation constant & 400 $\mu$M& \cite{Pich_2011_847}\\
\hline
$\mathrm{B0^{TRPN}_{tot}}$ & Total TroponinC concentration & 70 $\mu$M& \cite{Bond_2004_H1378}\\
\hline
$\mathrm{d_{tt}}$ & Distance from junctional t-tubule boundary & 40 nm& \\
\hline
$\sigma_{\mathrm{tt}}$ & Distance weight & 5 nm& \\
\hline
$\mathrm{k^{TRPN}_{off}}$ & TroponinC Ca$^{2+}$ off rate & $19.6\times 10 ^{-3}$ ms$^{-1}$& \cite{Bond_2004_H1378}\\
\hline
$\mathrm{k^{TRPN}_{on}}$ & TroponinC Ca$^{2+}$ on rate & $32.7\times 10 ^{-3}$ ms$^{-1}\,\mu$M$^{-1}$& \cite{Bond_2004_H1378}\\
\hline
$\mathrm{Kd^{TRPN}}$ & TroponinC Ca$^{2+}$ dissociation constant & $600\times 10 ^{-3}$ $\mu$M& \cite{Bond_2004_H1378}\\
\hline
\end{tabular}
\center\parbox{1.0\linewidth}{
        \vspace{-2em}
        \begin{description}
        \setlength{\leftmargin}{0em}
        \setlength{\rightmargin}{0em}
        \setlength{\itemindent}{-2em}
        \setlength{\labelsep}{.6em}
        \setlength{\itemsep}{0em}
        \setlength{\topsep}{0em}
        \item[$\star$ --] Calculated from a total ATP concentration of 5 mM together with a free \Mg concentration of 1 mM. A \Mg affinity for ATP of 200 $\mu$M is also assumed \cite{Bers_2001_book}.
        \item[$\dagger$ --] Swept in numerical experiments. Assumed to be the presented value for all other experiments. This gives a total number of \Ca binding sites of 219 $\mu$mole/$\ell$ cytosolic \cite{Bers_2001_book}.
        \item[$\ddagger$ --] When CSQN is distributed in all Z-line SR compartments we keep the same total amount of CSQN as when it is only found in jSR, see Table 1{ in supplementary methods section}.
        \item[$\S$ --] Swept in numerical experiments. Assumed to be the same as $\mathrm{k^{Fluo4}_{off}}$ for all other experiments.
        \item[$\P$ --] Fitted to D$^{\mathrm{cyt}}_{\mathrm{Ca}}$/3, together with the distance to nSR-bulk so the passive refill time constant is 212 ms, \cite{Zima_2008_105}
        \end{description}
        }
        
\end{table}


\renewcommand{\arraystretch}{1.0}
\begin{table}\scriptsize%\footnotesize
\center
\caption{\textbf{Initial values}}
\label{tab:initial-values}
\begin{tabular}{|l|l|r|}
\hline
\textbf{Domains}&\textbf{Species}&\textbf{Initial value}$^{\star}$\\
\hline
Cytosol & Ca & $140\times 10 ^{-3}$ $\mu$M\\
\cline{2-3}
& CaATP & $318\times 10 ^{-3}$ $\mu$M\\
\cline{2-3}
& CaCMDN & $471\times 10 ^{-3}$ $\mu$M\\
\cline{2-3}
& CaFluo & 2.82 $\mu$M\\
\cline{2-3}
& CaTRPN & 13.2 $\mu$M\\
\hline
SR & Ca & $1.3\times 10 ^{3}$ $\mu$M\\
\cline{2-3}
& CaCSQN & $20.1\times 10 ^{3}$ $\mu$M$^{\dagger}$\\
\cline{2-3}
& CaCSQN & $4.28\times 10 ^{3}$ $\mu$M$^{\ddagger}$\\
\cline{2-3}
& CaFluo5 & 19.1 $\mu$M\\
\hline
\end{tabular}
\center\parbox{1.0\linewidth}{
        \vspace{-2em}
        \begin{description}
        \setlength{\leftmargin}{0em}
        \setlength{\rightmargin}{0em}
        \setlength{\itemindent}{-1.5em}
        \setlength{\itemsep}{0em}
        \setlength{\labelsep}{.8em}
        \setlength{\topsep}{0em}
        \item[$\star$ --] All initial values of the included buffers are assumed to be in steady state with \Ca
        \item[$\dagger$ --] When CSQN is only in jSR
        \item[$\ddagger$ --] When CSQN is distributed in all Z-line compartments, see Table 1{ in supplementary methods section}. 
        \end{description}
        }
        
\end{table}

\clearpage
\bibliographystyle{jphysiol}
\bibliography{heartbib}


\end{document}
